# HostGhost

[![pipeline status](https://gitlab.com/localg-host/hostghost/badges/master/pipeline.svg)](https://gitlab.com/localg-host/hostghost/commits/master)
[![python test coverage](https://gitlab.com/localg-host/hostghost/badges/master/coverage.svg)](https://gitlab.com/localg-host/hostghost/-/pipelines)

Sensors monitoring and automation tool

Documentation: [localg-host.gitlab.io/hostghost](https://localg-host.gitlab.io/hostghost/)
