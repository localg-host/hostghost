DarkSky
#######

An API key is needed to use this sensor (see `DarkSky.net <https://darksky.net/dev/>`__).

Enter it in hostghost.service file.

- the minimal configuration is:

.. code-block:: toml

    [external_sensor_1]
    name = "Weather"
    sensor = "DARKSKY"
    type = "ext_temp"
    delay = 3600
    latitude = 45.7568
    longitude = 4.8309

:latitude: latitude
:longitude: longitude


- the values for thresholds are:
     - temperature
     - humidity



