AM2302
######

- the minimal configuration is:

.. code-block:: toml

    [local_sensor_1]
    name = "Temp sensor 1"
    sensor = "AM2302"
    type = "local_temp"
    delay = 60
    gpio = 4

:gpio: gpio pin used to connect DATA pin to Raspberry Pi


- the values for thresholds are:
     - temperature
     - humidity

example:

.. code-block:: toml

    [local_sensor_1]
    name = "Temp sensor 1"
    sensor = "AM2302"
    type = "local_temp"
    delay = 60
    gpio = 4
      [local_sensor_1.thresholds.temperature.max]
      value = 24
