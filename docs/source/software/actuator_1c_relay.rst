One-channel Relay
#################

- the configuration is:

.. code-block:: toml

    [local_actuator_1]
    name = "Relay 1"
    actuator = "RELAY1C"
    type = "local_relay_off"
    gpio = 17

:gpio: gpio pin used to connect IN pin to Raspberry Pi


- the actuator can be trigger by a sensor:

Example:

.. code-block:: toml

    [local_sensor_1]
    name = "Temp sensor 1"
    sensor = "AM2302"
    type = "local_temp"
    delay = 60
    gpio = 4
      [local_sensor_1.thresholds.temperature.max]
      value = 30
      actuator = "local_actuator_1"

The actuator **local_actuator_1** is triggered when the temperature measured by **local_sensor_1** is greater than or equal to 30°C.
