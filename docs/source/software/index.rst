Software
========

Installation
------------

Requirements
^^^^^^^^^^^^

Hostghost needs Python3.6+ and InfluxDB.


Install source from git
^^^^^^^^^^^^^^^^^^^^^^^

- Install **Hostghost** with required dependencies:

.. code:: bash

    $ git clone https://gitlab.com/localg-host/hostghost.git
    $ cd hostghost
    $ pip install -e .[adafruit,forecast_io,relay]
    $ python -m hostghost


Install HostGhost service
^^^^^^^^^^^^^^^^^^^^^^^^^

- Update ``hostghost.service`` if needed (see Environement variables below) and copy it in systemd directory:

.. code:: bash

    $ nano service/hostghost.service
    $ sudo cp service/hostghost.service /etc/systemd/system/hostghost.service

- Start and enable the service

.. code:: bash

    $ sudo systemctl start hostghost
    $ sudo systemctl enable hostghost


HostGhost configuration
-----------------------

HostGhost can be configured with the following environment variables:

.. cssclass:: table-bordered table-striped

========================== ======================================= ===============================================================
variable                   description                             app default value
========================== ======================================= ===============================================================
``HOSTGHOST_HOST``         Application host                        '0.0.0.0'
``HOSTGHOST_PORT``         Application port                        8889
``HOSTGHOST_UI_URL``       Application URL                         no defaut value, must be initialized to received notifications
``HOSTGHOST_INFLUX_HOST``  InfluxDB host                           'localhost'
``HOSTGHOST_INFLUX_PORT``  InfluxDB port                           8086
``HOSTGHOST_LOG``          Path to the log file                    no defaut value
``DARK_SKY_API``           DarkSky API key                         no defaut value, must be initialized to use DarkSky sensor
``EMAIL_URL``              Email URL with credentials              no defaut value, must be initialized to received notifications
``SENDER_EMAIL``           Application sender email address        'no-reply@example.com'
``RECIPIENT_EMAIL``        Notification recipient email address    'no-reply@example.com'
========================== ======================================= ===============================================================


Update HostGhost
----------------

To update, re-run pip install:

.. code:: bash

    $ cd hostghost
    $ git pull
    $ pip install -e .[adafruit,forecast_io,relay]


Email notifications
-------------------

- to activate notifications for sensors, a valid email URL must be provided (see environment variable ``EMAIL_URL``):

  - with an unencrypted SMTP server: ``smtp://username:password@smtp.example.com:25``
  - with SSL: ``smtp://username:password@smtp.example.com:465/?ssl=True``
  - with STARTTLS: ``smtp://username:password@smtp.example.com:587/?tls=True``

- a notification is sent when:

  - a value reaches a defined threshold for the first time,
  - a value drops below the defined threshold.

- notifications can be disabled for a specific sensor (see sensor configuration).

Device configuration
--------------------

Sensors and actuators configuration is stored ``~/.config/hostghost``, in TOML files.


Sensors
^^^^^^^

All sensors configuration is saved in ``sensors.toml``.

- minimal configuration

.. code-block:: toml

    [sensor_id]
    name = "sensor_name"
    sensor = "SENSOR_TYPE"
    type = "xxx"
    delay = 60
    notifications = false

:sensor_id: device id, must be unique across all TOML file.
:name: name displayed on hostghost
:sensor: supported sensor
:type: can be "**local_temp**", "**ext_temp**", "**local_energy**"
:delay: delay in seconds (default value: 60 seconds)
:notifications: allow to receive email notifications (default value: true)


- with thresholds (=> display a marker on chart)

.. code-block:: toml

    [sensor_id]
    name = "sensor_name"
    sensor = "SENSOR_TYPE"
    type = "xxx"
    delay = 60
      [sensor_id.thresholds.value_name.threshold_type]
      value = 20

:value_name: name of the value (example: temperature with temperature sensor)
:threshold_type: can be "**min**" or "**max**"
:value: threshold value


- with thresholds (=> display a region on chart)

.. code-block:: toml

    [sensor_id]
    name = "sensor_name"
    sensor = "SENSOR_TYPE"
    type = "xxx"
    delay = 60
      [sensor_id.thresholds.value_name.max]
      value = 30
      [sensor_id.thresholds.value_name.min]
      value = 10

- with threshold that triggers an actuator

.. code-block:: toml

    [sensor_id]
    name = "sensor_name"
    sensor = "SENSOR_TYPE"
    type = "xxx"
    delay = 60
      [sensor_id.thresholds.value_name.threshold_type]
      value = 20
      actuator = "local_actuator_1"

:actuator: actuator id

Supported sensors:

.. toctree::
   :maxdepth: 2

   sensor_am2302
   sensor_darksky
   sensor_pzem004t


Actuators
^^^^^^^^^

All actuators configuration is saved in ``actuators.toml``.

Actuators can only have two states:

- 'ON' (= True)
- 'OFF' (= False)

They don't need a delay unlike sensors.


- configuration:

.. code-block:: toml

    [actuator_id]
    name = "actuator_name"
    actuator = "ACTUATOR_TYPE"
    type = "xxx"

:actuator_id: device id, must be unique across all TOML file.
:name: name displayed on hostghost
:actuator: supported actuator
:type: default state, can be 'local_relay_off', 'local_relay_on'

Supported actuators:

.. toctree::
   :maxdepth: 2

   actuator_1c_relay
