PZEM-004T
#########

- the minimal configuration is:

.. code-block:: toml

    [local_sensor_2]
    name = "Energy sensor 1"
    sensor = "PZEM004T"
    type = "local_energy"
    com = "/dev/ttyUSB0"
    delay = 10

:com: interface used to connect the energy monitor

To determine the interface, use ``dmesg`` command:

.. code-block:: bash

    pi@raspberrypi:~ $ dmesg | grep tty
    [    0.000000] Kernel command line: coherent_pool=1M bcm2708_fb.fbwidth=720 bcm2708_fb.fbheight=480 bcm2708_fb.fbswap=1 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  dwc_otg.lpm_enable=0 console=ttyAMA0,115200 console=tty1 root=PARTUUID=81d34d9c-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles
    [    0.000376] console [tty1] enabled
    [    0.796408] 3f201000.serial: ttyAMA0 at MMIO 0x3f201000 (irq = 81, base_baud = 0) is a PL011 rev2
    [    0.796478] console [ttyAMA0] enabled
    [    2.310633] systemd[1]: Created slice system-serial\x2dgetty.slice.
    [    3.484291] usb 1-1.4: pl2303 converter now attached to ttyUSB0

In this case, the energy monitor is connected to **ttyUSB0**.

- the values for thresholds are:
     - current
     - power
     - voltage
