.. HostGhost documentation master file, created by
   sphinx-quickstart on Wed Aug  7 21:02:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

HostGhost
=========

HostGhost is a monitoring and automation application, designed to run on a Raspberry Pi.

The following sensors and actuators are currently supported:

- AM2302 temperature-humidity sensor
- PZEM-004T energy sensor
- DarkSky weather (former forecast.io)
- One-channel relay


.. warning::

   documentation in progress


Table of contents
^^^^^^^^^^^^^^^^^
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   hardware/index.rst
   software/index.rst
   changelog
