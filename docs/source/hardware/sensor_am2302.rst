AM2302
######

Description
-----------

This sensors, based on DHT22, measures temperature and humidity.

Specs (from `Adafruit <https://www.adafruit.com/product/393>`__ website):

- 3 to 5V power and I/O
- 2.5mA max current use during conversion (while requesting data)
- 0-100% humidity readings with 2-5% accuracy
- 40 to 80°C temperature readings ±0.5°C accuracy


Connection to Raspberry Pi
--------------------------

.. cssclass:: table-bordered table-striped

====== ============
AM2302 Raspberry Pi
====== ============
GND    GND
Data   GPIO4
VDD    3.3v
====== ============

.. figure:: images/AM2302.png
   :alt: AM2302

   AM2302 connection to Raspberry Pi
