Hardware
========

Raspberry Pi preparation
------------------------

-  Get the latest Raspbian image:
   https://www.raspberrypi.org/downloads/raspbian/
-  Copy the downloaded image on a SD Card (min. 8Go).
-  Before removing the SD card, `activate
   SSH <https://www.raspberrypi.org/documentation/remote-access/ssh/README.md>`__
   by adding a file named ``ssh`` on boot partition.
-  To directly use `Wifi
   connection <https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md>`__,
   edit ``wpa-supplicant`` configuration:

.. code:: bash

   $ sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

-  After switching on the Raspberry Pi

.. code:: bash

   pi@raspberrypi:~ $ sudo apt-get update
   pi@raspberrypi:~ $ sudo apt-get upgrade

-  [Optional] Some dependencies need Python 3.6+. Depending on Raspbian version, update Python version if needed.

   Example: Python 3.6.9 installation

.. code:: bash

   pi@raspberrypi:~ $ sudo apt-get install python3-dev libffi-dev libssl-dev
   pi@raspberrypi:~ $ wget https://www.python.org/ftp/python/3.6.9/Python-3.6.9.tar.xz
   pi@raspberrypi:~ $ tar xJf Python-3.6.9.tar.xz
   pi@raspberrypi:~ $ cd Python-3.6.9
   pi@raspberrypi:~/Python-3.6.9 $ ./configure
   pi@raspberrypi:~/Python-3.6.9 $ make -j4  # -j4 allows to use 4 cores (much faster)
   pi@raspberrypi:~/Python-3.6.9 $ sudo make install

.. note::
    The latest Raspbian (Buster) is shipped with 3.7.3. This step can be skipped

-  Reboot the Raspberry Pi
-  Install InfluxDB

.. code:: bash

   pi@raspberrypi:~ $ curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
   pi@raspberrypi:~ $ echo "deb https://repos.influxdata.com/debian stretch stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
   pi@raspberrypi:~ $ sudo apt-get update
   pi@raspberrypi:~ $ sudo apt-get install influxdb
   pi@raspberrypi:~ $ sudo systemctl enable influxdb
   pi@raspberrypi:~ $ sudo systemctl start influxdb


Sensors
-------

.. toctree::
   :maxdepth: 2

   sensor_am2302
   sensor_pzem004t


Actuators
---------

.. toctree::
   :maxdepth: 2

   actuator_1c_relay
