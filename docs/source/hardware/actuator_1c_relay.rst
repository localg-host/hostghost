One-channel Relay
#################

Description
-----------

HostGhost supports only one channel relay for now.

.. danger::

    Be careful, because of the risk of electrocution.

    **Basic knowledge is required before handling this kind of material.**


Connection to Raspberry Pi
--------------------------

.. cssclass:: table-bordered table-striped

====== ============
Relay  Raspberry Pi
====== ============
GND    GND
IN     GPIO17
VDD    3.3v
====== ============

.. figure:: images/1C_Relay.png
   :alt: Relay

   Relay connection to Raspberry Pi
