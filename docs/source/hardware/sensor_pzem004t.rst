PZEM-004T
#########

Description
-----------

PZEM-004T is an non-invasive energy monitor, that measures:

- voltage
- amperage
- power

.. danger::

    Be careful, because of the risk of electrocution.

    **Basic knowledge is required before handling this kind of material.**


Connection to Raspberry Pi
--------------------------

PZEM-004T is provided with USB cable

.. figure:: images/PZEM004T.jpg
   :alt: PZEM004T

   PZEM004T connection to Raspberry Pi
