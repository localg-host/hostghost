build:
	cd hostghost_front && yarn run lint && yarn run build && git add --all

html:
	rm -rf docs/build
	sphinx-build -b html docs/source docs/build
	rm -rf docs/build/_static/bootstrap-2.3.2
	rm -rf docs/build/_static/bootswatch-2.3.2
	find docs/build/_static/bootswatch-3.3.7/. -maxdepth 1 -not -name darkly -not -name fonts -exec rm -rf '{}' \; 2>/tmp/NULL
	sed -i "s/\@import url(\"https:\/\/fonts.googleapis.com\/css?family=Lato:400,700,400italic\");//" docs/build/_static/bootswatch-3.3.7/darkly/bootstrap.min.css
