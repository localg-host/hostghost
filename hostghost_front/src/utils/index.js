const chartLabelsColors = {
  temperature_min: {
    label: 'min. (°C)',
    color: '#c16666',
  },
  temperature_max: {
    label: 'max. (°C)',
    color: '#c16666',
  },
  temperature_mean: {
    label: 'mean (°C)',
    color: '#990000',
  },
  humidity_min: {
    label: 'min. (%)',
    color: '#66c1c1',
  },
  humidity_max: {
    label: 'max. (%)',
    color: '#66c1c1',
  },
  humidity_mean: {
    label: 'mean (%)',
    color: '#009999',
  },
  energy: {
    label: 'energy (kWh)',
    color: '#f4f22d',
  },
  power_min: {
    label: 'min (W)',
    color: '#a3751b',
  },
  power_mean: {
    label: 'mean (W)',
    color: '#e9a827',
  },
  power_max: {
    label: 'max (W)',
    color: '#a3751b',
  },
  active: {
    label: 'active (%)',
    color: '#ff9873',
  },
  inactive: {
    label: 'inactive (%)',
    color: '#575757',
  },
}

const hoursFormat = {
  unit: 'hour',
  displayFormats: {
    hour: 'h:mm a',
  },
  tooltipFormat: 'h:mm a',
}

const dayFormat = {
  unit: 'day',
  displayFormats: {
    day: 'DD/MM',
  },
  tooltipFormat: 'DD/MM/YYYY',
}

const weekFormat = {
  unit: 'week',
  displayFormats: {
    week: 'DD/MM',
  },
  tooltipFormat: 'DD/MM/YYYY',
}

const monthFormat = {
  unit: 'month',
  displayFormats: {
    week: 'MM/YYYY',
  },
  tooltipFormat: 'DD/MM/YYYY',
}

const dateFormats = {
  '24h': hoursFormat,
  '7d': dayFormat,
  '30d': dayFormat,
  '90d': weekFormat,
  '1y': monthFormat,
}

export const getChartType = type =>
  ['energy', 'active'].includes(type) ? 'bar' : 'line'

const makeTransparent = (color, opacity = 0.85) => {
  const alpha = 1 - opacity
  return Color(color).alpha(alpha).rgbString()
}

export const handleChartData = (sensorData, timeFrame) => {
  const labels = []
  const datasets = {}
  const dataTypes = []
  const dateFormat = dateFormats[timeFrame]
    ? dateFormats[timeFrame]
    : hoursFormat
  const minMaxValues = {}

  if (sensorData.series) {
    for (let i = 1; i < sensorData.series[0].columns.length; i++) {
      const typeLabel = sensorData.series[0].columns[i]
      const type = typeLabel.split('_')[0]
      if (!dataTypes.includes(type)) {
        dataTypes.push(type)
        minMaxValues[type] = {}
      }
      const chartType = getChartType(type)
      datasets[typeLabel] = {
        data: [],
        dataType: type,
        label: chartLabelsColors[typeLabel].label,
        type: chartType,
      }
      if (type === 'active') {
        datasets['inactive'] = {
          backgroundColor: chartLabelsColors[typeLabel].color,
          borderColor: chartLabelsColors[typeLabel].color,
          data: [],
          dataType: type,
          label: chartLabelsColors['inactive'].label,
          type: chartType,
        }
      } else {
        if (typeLabel.includes('mean') || chartType === 'bar') {
          datasets[typeLabel].borderColor = chartLabelsColors[typeLabel].color
          if (chartType === 'bar') {
            datasets[typeLabel].backgroundColor =
              chartLabelsColors[typeLabel].color
          } else {
            datasets[typeLabel].pointBackgroundColor =
              chartLabelsColors[typeLabel].color
          }
        } else {
          // min and max values
          datasets[typeLabel].backgroundColor = makeTransparent(
            chartLabelsColors[typeLabel].color
          )
          datasets[typeLabel].fill = +1
          datasets[typeLabel].pointBackgroundColor = 'transparent'
          datasets[typeLabel].pointBorderColor = 'transparent'
        }
      }
    }

    sensorData.series[0].values.forEach(value => {
      labels.push(new Date(value[0] * 1000))
      for (let i = 1; i < sensorData.series[0].columns.length; i++) {
        const typeLabel = sensorData.series[0].columns[i]
        if (typeLabel === 'active') {
          // 100 for state
          datasets[typeLabel]['data'].push(
            value[i] ? (value[i] * 100).toFixed(1) : null
          )
          datasets['inactive']['data'].push(
            value[i] ? ((1 - value[i]) * 100).toFixed(1) : null
          )
        } else {
          datasets[typeLabel]['data'].push(
            value[i] ? value[i].toFixed(2) : value[i]
          )
        }
      }
    })
    for (let typeLabel in datasets) {
      const [type, valueType] = typeLabel.split('_')
      const data = datasets[typeLabel].data.filter(d => d !== null)
      if (valueType === 'min') {
        minMaxValues[type][valueType] = Math.min(...data)
      }
      if (valueType === 'max') {
        minMaxValues[type][valueType] = Math.max(...data)
      }
    }
  }

  return {
    labels,
    datasets: Object.values(datasets),
    dataTypes,
    dateFormat,
    minMaxValues,
    ts: new Date().getTime(),
  }
}

export const handleError = (
  commit,
  err,
  msg = 'Error. Please try again or contact the administrator.'
) => {
  return commit(
    'setError',
    err.response
      ? err.response.data.message
        ? err.response.data.message
        : msg
      : err.message
      ? err.message
      : msg
  )
}
