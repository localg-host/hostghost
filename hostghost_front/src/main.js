import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'

import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

let wsScheme = 'ws'
if (window.location.protocol === 'https:') {
  wsScheme = 'wss'
}
const wsLocation = wsScheme + '://' + window.location.host + '/websocket'

Vue.use(VueNativeSock, wsLocation, {
  format: 'json',
  reconnection: true,
  store: store,
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
