import axios from 'axios'
import dayjs from 'dayjs'
import Vue from 'vue'
import Vuex from 'vuex'

import { handleChartData, handleError } from './utils'

Vue.use(Vuex)

const api = axios.create({
  baseURL: '/api',
})

export default new Vuex.Store({
  state: {
    devices: [],
    deviceData: {},
    error: null,
    socketIsConnected: false,
    timeFrame: '24h',
    start: null,
    end: null,
    hideThresholds: false,
  },
  getters: {
    device: state => slug => {
      return state.devices.find(device => device.slug === slug)
    },
    deviceData: state => {
      return state.deviceData
    },
    devices: state => {
      return state.devices
    },
    error: state => {
      return state.error
    },
    socketIsConnected: state => {
      return state.socketIsConnected
    },
    timeFrame: state => {
      return state.timeFrame
    },
    start: state => {
      return state.start
    },
    end: state => {
      return state.end
    },
    hideThresholds: state => {
      return state.hideThresholds
    },
  },
  mutations: {
    setError(state, error) {
      state.error = error
    },
    updateDevices(state, devices) {
      state.devices = devices
    },
    updateDeviceData(state, data) {
      state.deviceData = data
    },
    updateTimeFrame(state, data) {
      state.timeFrame = data
    },
    updateStartEnd(state, data) {
      state.start = dayjs(new Date(data.start * 1000))
      state.end = dayjs(new Date(data.end * 1000))
    },
    updateHideThresholds(state, hideThresholds) {
      state.hideThresholds = hideThresholds
    },
    SOCKET_ONOPEN(state, event) {
      Vue.prototype.$socket = event.currentTarget
      state.socketIsConnected = true
      document.title = 'HostGhost'
    },
    SOCKET_ONCLOSE(state) {
      state.socketIsConnected = false
      document.title = 'HostGhost ⚠️'
      state.devices = state.devices.map(device => {
        device.values = null
        return device
      })
    },
    SOCKET_ONMESSAGE(state, message) {
      const device_slug = Object.keys(message)[0]
      state.devices = state.devices.map(device => {
        if (device.slug === device_slug) {
          device.values = message[device_slug]
        }
        return device
      })
    },
  },
  actions: {
    getDevices({ commit }) {
      api
        .get(`devices/`)
        .then(res => {
          if (res.status === 200) {
            commit('updateDevices', res.data.devices || [])
          }
        })
        .catch(err => handleError(commit, err))
    },
    getDeviceData({ commit }, data) {
      api
        .get(`devices/${data.slug}/data/${data.query ? data.query : ''}`)
        .then(res => {
          if (res.status === 200 && res.data.data.results) {
            commit(
              'updateDeviceData',
              handleChartData(res.data.data.results[0], data.timeFrame)
            )
            commit('updateStartEnd', {
              start: res.data.start,
              end: res.data.end,
            })
          }
        })
        .catch(err => handleError(commit, err))
    },
  },
})
