const path = require('path')

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    performance: {
      maxEntrypointSize: 400000,
      maxAssetSize: 500000,
    },
    externals: {
      moment: 'moment',
    },
  },
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/static/',
  assetsDir: '../static',
  outputDir: path.resolve(__dirname, '../hostghost/static'),
  indexPath: '../templates/index.html',
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8889',
      },
      '/websocket': {
        target: 'ws://localhost:8889',
        ws: true,
      },
    },
  },
}
