# flake8: noqa

message_below_threshold = {
    'name': 'Temp sensor 1',
    'previous_threshold': 'max',
    'threshold_exceeded': False,
    'slug': 'local-sensor-1',
    'threshold': 'max',
    'threshold_value': 30,
    'ui_url': 'http://0.0.0.0:8883',
    'value': 29,
    'value_type': 'temperature',
}


message_over_threshold = {
    'name': 'Temp sensor 1',
    'previous_threshold': None,
    'threshold_exceeded': True,
    'slug': 'local-sensor-1',
    'threshold': 'max',
    'threshold_value': 30,
    'ui_url': 'http://0.0.0.0:8883',
    'value': 32,
    'value_type': 'temperature',
}


updated_message_data_below_threshold = {
    'name': 'Temp sensor 1',
    'previous_threshold': 'max',
    'threshold_exceeded': False,
    'threshold_action': 'drops below',
    'slug': 'local-sensor-1',
    'threshold': 'max',
    'threshold_value': 30,
    'ui_url': 'http://0.0.0.0:8883',
    'value': 29,
    'value_type': 'temperature',
}


updated_message_data_over_threshold = {
    'name': 'Temp sensor 1',
    'previous_threshold': None,
    'threshold_exceeded': True,
    'threshold_action': 'reaches',
    'slug': 'local-sensor-1',
    'threshold': 'max',
    'threshold_value': 30,
    'ui_url': 'http://0.0.0.0:8883',
    'value': 32,
    'value_type': 'temperature',
}


expected_subject_over_threshold = (
    "[HostGhost] temperature reaches threshold for sensor 'Temp sensor 1'"
)
expected_message_over_threshold = """Hi,

The current temperature returned by sensor 'Temp sensor 1' reaches the defined threshold:
- current value: 32,
- threshold 'max': 30.

Sensor details: http://0.0.0.0:8883/devices/local-sensor-1

HostGhost
"""

expected_subject_below_threshold = (
    "[HostGhost] temperature drops below threshold for sensor 'Temp sensor 1'"
)
expected_message_below_threshold = """Hi,

The current temperature returned by sensor 'Temp sensor 1' drops below the defined threshold:
- current value: 29,
- threshold 'max': 30.

Sensor details: http://0.0.0.0:8883/devices/local-sensor-1

HostGhost
"""
