from copy import copy
from unittest.mock import patch

import pytest

from hostghost.devices.sensors.sensor import Sensor
from hostghost.devices.sensors.temperature_humidity.adafruit import (
    AdafruitSensor,
)
from hostghost.devices.sensors.temperature_humidity.dark_sky import (
    DarkSkySensor,
)
from hostghost.devices.sensors.temperature_humidity.temp_sensor import (
    TempHumiditySensor,
)
from hostghost.exceptions import InvalidApiKey

DEFAULT_CONFIG = {
    'delay': 60,
    'gpio': 4,
    'name': 'Temp sensor 1',
    'sensor': 'NOT_WORKING',
    'type': 'local_temp',
}


def get_sensor_with_max_threshold():
    config = copy(DEFAULT_CONFIG)
    config['thresholds'] = {'temperature': {'max': {'value': 30}}}
    return Sensor(name='temp_sensor_1', config=config)


def get_sensor_with_min_threshold():
    config = copy(DEFAULT_CONFIG)
    config['thresholds'] = {'temperature': {'min': {'value': 30}}}
    return Sensor(name='temp_sensor_1', config=config)


def get_sensor_with_max_threshold_and_actuator():
    config = copy(DEFAULT_CONFIG)
    config['thresholds'] = {
        'temperature': {'max': {'value': 30, 'actuator': 'local_actuator_1'}}
    }
    return Sensor(name='temp_sensor_1', config=config)


def get_sensor_with_min_threshold_and_actuator():
    config = copy(DEFAULT_CONFIG)
    config['thresholds'] = {
        'temperature': {'min': {'value': 30, 'actuator': 'local_actuator_1'}}
    }
    return Sensor(name='temp_sensor_1', config=config)


class TestSensor:
    @staticmethod
    def get_sensor():
        return Sensor(name='temp_sensor_1', config=DEFAULT_CONFIG)

    def test_sensor_serialization(self, event_loop):
        sensor = self.get_sensor()
        serialized_sensor = sensor.serialize()
        assert serialized_sensor['name'] == 'Temp sensor 1'
        assert serialized_sensor['device_type'] == 'sensor'
        assert serialized_sensor['delay'] == 60
        assert serialized_sensor['gpio'] == 4
        assert serialized_sensor['sensor'] == 'NOT_WORKING'
        assert serialized_sensor['type'] == 'local_temp'
        assert serialized_sensor['slug'] == 'temp-sensor-1'
        assert serialized_sensor['values'] is None

    @pytest.mark.xfail(raises=NotImplementedError)
    async def test_it_raises_error_if_get_data_not_defined(self, event_loop):
        not_working_sensor = self.get_sensor()

        await not_working_sensor.get_data()

    @pytest.mark.xfail(raises=NotImplementedError)
    async def test_it_raises_error_if_get_graph_data_not_defined(
        self, event_loop
    ):
        not_working_sensor = self.get_sensor()

        await not_working_sensor.get_graph_data(None, None, None)

    async def test_it_returns_no_threshold_exceeded_on_sensor_without_threshold(  # noqa
        self,
    ):
        sensor = self.get_sensor()

        result = await sensor.check_thresholds(
            value=20, value_type='temperature'
        )

        assert result == {'exceeds_thresholds': None}
        assert sensor.threshold_exceeded == {}

    @pytest.mark.parametrize(
        "input_sensor,input_value",
        [
            (get_sensor_with_min_threshold(), 40),
            (get_sensor_with_max_threshold(), 20),
        ],
    )
    async def test_it_returns_no_threshold_exceeded_on_sensor_with_threshold(
        self, input_sensor, input_value
    ):
        sensor = input_sensor

        result = await sensor.check_thresholds(
            value=input_value, value_type='temperature'
        )

        assert result == {'exceeds_thresholds': None}
        assert sensor.threshold_exceeded == {}

    @pytest.mark.parametrize(
        "input_sensor,input_value,expected_result,expected_threshold",
        [
            (
                get_sensor_with_min_threshold(),
                20,
                {'exceeds_thresholds': 'min'},
                {'temperature': 'min'},
            ),
            (
                get_sensor_with_max_threshold(),
                40,
                {'exceeds_thresholds': 'max'},
                {'temperature': 'max'},
            ),
            (
                get_sensor_with_min_threshold_and_actuator(),
                20,
                {'action': 'local_actuator_1', 'exceeds_thresholds': 'min'},
                {'temperature': 'min'},
            ),
            (
                get_sensor_with_max_threshold_and_actuator(),
                40,
                {'action': 'local_actuator_1', 'exceeds_thresholds': 'max'},
                {'temperature': 'max'},
            ),
        ],
    )
    async def test_it_returns_threshold_exceeded_with_sensor_with_threshold(
        self, input_sensor, input_value, expected_result, expected_threshold
    ):
        sensor = input_sensor

        result = await sensor.check_thresholds(
            value=input_value, value_type='temperature'
        )

        assert result == expected_result
        assert sensor.threshold_exceeded == expected_threshold

    @pytest.mark.parametrize(
        "input_sensor,expected_result",
        [
            (get_sensor_with_max_threshold(), {'exceeds_thresholds': 'max'},),
            (
                get_sensor_with_max_threshold_and_actuator(),
                {'action': 'local_actuator_1', 'exceeds_thresholds': 'max'},
            ),
        ],
    )
    async def test_it_updates_threshold_when_value_reaches_threshold(
        self, input_sensor, expected_result
    ):
        sensor = input_sensor
        sensor.threshold_exceeded = {'humidity': 'max'}

        result = await sensor.check_thresholds(
            value=40, value_type='temperature'
        )

        assert result == expected_result
        assert sensor.threshold_exceeded == {
            'humidity': 'max',
            'temperature': 'max',
        }

    @pytest.mark.parametrize(
        "input_sensor,input_value",
        [
            (get_sensor_with_min_threshold(), 20,),
            (get_sensor_with_max_threshold(), 40,),
            (get_sensor_with_min_threshold_and_actuator(), 20,),
            (get_sensor_with_max_threshold_and_actuator(), 40,),
        ],
    )
    async def test_it_sends_notification_when_threshold_is_reached(
        self, app_mock_smtp, input_sensor, input_value,
    ):
        sensor = input_sensor

        await sensor.check_thresholds(
            value=input_value, value_type='temperature'
        )

        assert app_mock_smtp.return_value.send_message.call_count == 1

    @patch('hostghost.devices.sensors.sensor.app')
    async def test_it_does_not_send_notification_when_app_email_is_not_defined(
        self, app, mock_smtp,
    ):
        sensor = get_sensor_with_min_threshold()
        await sensor.check_thresholds(value=0, value_type='temperature')

        assert mock_smtp.return_value.send_message.call_count == 0

    @patch('hostghost.devices.sensors.sensor.app')
    async def test_it_does_not_send_notification_when_notifications_are_disabled(  # noqa
        self, app_mock_smtp, mock_smtp,
    ):
        sensor = get_sensor_with_min_threshold()
        sensor.config['notifications'] = False
        await sensor.check_thresholds(value=0, value_type='temperature')

        assert mock_smtp.return_value.send_message.call_count == 0

    @pytest.mark.parametrize(
        "input_sensor,input_value,input_threshold,expected_result",
        [
            (
                get_sensor_with_min_threshold(),
                40,
                {'temperature': 'min'},
                {'exceeds_thresholds': None},
            ),
            (
                get_sensor_with_max_threshold(),
                20,
                {'temperature': 'max'},
                {'exceeds_thresholds': None},
            ),
            (
                get_sensor_with_min_threshold_and_actuator(),
                40,
                {'temperature': 'min'},
                {'action': 'local_actuator_1', 'exceeds_thresholds': None},
            ),
            (
                get_sensor_with_max_threshold_and_actuator(),
                20,
                {'temperature': 'max'},
                {'action': 'local_actuator_1', 'exceeds_thresholds': None},
            ),
        ],
    )
    async def test_it_empties_threshold_when_value_is_below_threshold(
        self,
        event_loop,
        input_sensor,
        input_value,
        input_threshold,
        expected_result,
    ):
        sensor = input_sensor
        sensor.threshold_exceeded = input_threshold

        result = await sensor.check_thresholds(
            value=input_value, value_type='temperature'
        )

        assert result == expected_result
        assert sensor.threshold_exceeded == {}

    @pytest.mark.parametrize(
        "input_sensor,expected_result",
        [
            (get_sensor_with_max_threshold(), {'exceeds_thresholds': None},),
            (
                get_sensor_with_max_threshold_and_actuator(),
                {'action': 'local_actuator_1', 'exceeds_thresholds': None},
            ),
        ],
    )
    async def test_it_updates_threshold_when_value_is_below_threshold(
        self, event_loop, input_sensor, expected_result
    ):
        sensor = input_sensor
        sensor.threshold_exceeded = {'temperature': 'max', 'humidity': 'max'}

        result = await sensor.check_thresholds(
            value=0, value_type='temperature'
        )

        assert result == expected_result
        assert sensor.threshold_exceeded == {'humidity': 'max'}

    @pytest.mark.parametrize(
        "input_sensor,input_value,input_threshold",
        [
            (get_sensor_with_min_threshold(), 40, {'temperature': 'min'},),
            (get_sensor_with_max_threshold(), 20, {'temperature': 'max'},),
            (
                get_sensor_with_min_threshold_and_actuator(),
                40,
                {'temperature': 'min'},
            ),
            (
                get_sensor_with_max_threshold_and_actuator(),
                20,
                {'temperature': 'max'},
            ),
        ],
    )
    async def test_it_sends_email_when_value_is_below_threshold(
        self, app_mock_smtp, input_sensor, input_value, input_threshold
    ):
        sensor = input_sensor
        sensor.threshold_exceeded = input_threshold

        await sensor.check_thresholds(
            value=input_value, value_type='temperature'
        )

        assert app_mock_smtp.return_value.send_message.call_count == 1

    async def test_it_triggers_actuator(self, event_loop, app_with_actuator):
        sensor = get_sensor_with_max_threshold_and_actuator()
        actuator = app_with_actuator

        await sensor.trigger_actuator('local_actuator_1')

        assert actuator.is_active


class TestTempHumiditySensor:
    @staticmethod
    def get_sensor():
        return TempHumiditySensor(
            name='local_sensor_1',
            config={
                'name': 'Temp sensor 1',
                'actuator': 'DUMMY_SENSOR',
                'type': 'local_temp',
            },
        )

    @pytest.mark.xfail(raises=NotImplementedError)
    async def test_it_raises_error_if_get_temp_humidity_not_defined(
        self, event_loop
    ):
        sensor = self.get_sensor()
        await sensor.get_temp_humidity(None, None)


class TestAdafruitSensor:
    @staticmethod
    def get_sensor(pin=4):
        # pin 12 returns invalid values
        return AdafruitSensor(
            name='local_sensor_1',
            config={
                'name': 'Temp sensor 1',
                'actuator': 'AM2302',
                'type': 'local_temp',
                'gpio': pin,
            },
        )

    async def test_it_gets_temperature_and_humidity(self, event_loop):
        sensor = self.get_sensor()
        humidity, temperature = await sensor.get_temp_humidity(None, None)
        assert temperature == 30
        assert humidity == 40

    async def test_it_does_not_save_temperature_and_humidity_if_invalid(
        self, event_loop, caplog
    ):
        sensor = self.get_sensor(pin=12)
        await sensor.get_data()
        assert len(caplog.records) == 1
        assert caplog.records[0].levelname == 'ERROR'
        assert (
            caplog.records[0].message == (
                "[get_data] - Sensor 'local_sensor_1': invalid humidity "
                "value (120.0%), data not saved."
            )
        )


class TestDarkSkySensor:
    @staticmethod
    def get_sensor():
        return DarkSkySensor(
            name='local_sensor_1',
            config={
                'name': 'Weather',
                'actuator': 'DARKSKY',
                'type': 'ext_temp',
                'delay': 3600,
                'latitude': 45.7568,
                'longitude': 4.8309,
            },
        )

    async def test_it_raises_if_no_api_key_provided(self, event_loop):
        sensor = self.get_sensor()
        with pytest.raises(
            InvalidApiKey, match='No api Key for weather sensor'
        ):
            await sensor.get_temp_humidity(None, None)

    @patch(
        'hostghost.devices.sensors.temperature_humidity.dark_sky.API_KEY',
        'api_key_example',
    )
    async def test_it_gets_temperature_and_humidity(self, event_loop):
        sensor = self.get_sensor()
        humidity, temperature = await sensor.get_temp_humidity(None, None)
        assert temperature == 25
        assert humidity == 35
