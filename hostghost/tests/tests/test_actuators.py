import pytest

from hostghost.devices.actuators.actuator import Actuator
from hostghost.devices.actuators.relay.dummy import DummyRelay
from hostghost.devices.actuators.relay.one_channel_relay import OneChannelRelay


class TestActuator:
    @staticmethod
    def get_actuator():
        return Actuator(
            name='relay_1',
            config={
                'gpio': 4,
                'name': 'Relay 1',
                'actuator': 'NOT_WORKING',
                'type': 'local_relay_off',
            },
        )

    def test_actuator_serialization(self, event_loop):
        actuator = self.get_actuator()
        serialized_relay = actuator.serialize()
        assert serialized_relay['name'] == 'Relay 1'
        assert serialized_relay['device_type'] == 'actuator'
        assert serialized_relay['gpio'] == 4
        assert serialized_relay['actuator'] == 'NOT_WORKING'
        assert serialized_relay['type'] == 'local_relay_off'
        assert serialized_relay['slug'] == 'relay-1'
        assert serialized_relay['values'] is None

    @pytest.mark.xfail(raises=NotImplementedError)
    async def test_it_raises_error_if_get_data_not_defined(self, event_loop):
        not_working_actuator = self.get_actuator()
        await not_working_actuator.get_data()

    @pytest.mark.xfail(raises=NotImplementedError)
    async def test_it_raises_error_if_action_actuator_not_defined(
        self, event_loop
    ):
        not_working_actuator = self.get_actuator()
        await not_working_actuator.action_actuator()


class TestDummyRelay:
    @staticmethod
    def get_relay():
        return DummyRelay(
            name='Relay 1',
            config={
                'name': 'Relay 1',
                'actuator': 'DUMMY_RELAY',
                'type': 'local_relay_off',
            },
        )

    def test_default_state_is_not_active(self, event_loop):
        dummy_relay = self.get_relay()

        assert not dummy_relay.is_active

    async def test_it_activate_actuator(self, event_loop):
        dummy_relay = self.get_relay()

        await dummy_relay.action_actuator()

        assert dummy_relay.is_active


class TestOneChannelRelay:
    @staticmethod
    def get_relay():
        return OneChannelRelay(
            name='Relay 1',
            config={
                'gpio': 17,
                'name': 'Relay 1',
                'actuator': 'RELAY1C',
                'type': 'local_relay_off',
            },
        )

    def test_default_state_is_not_active(self, event_loop):
        relay = self.get_relay()

        assert relay.is_active == 0

    async def test_it_activate_actuator(self, event_loop):
        relay = self.get_relay()

        await relay.action_actuator()

        assert relay.is_active == 1
