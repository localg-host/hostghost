from datetime import datetime

import pytest

from hostghost.utils import subtract_year


class TestUtils:
    @pytest.mark.parametrize(
        "input_date,expected_date",
        [
            ('2020-01-01', '2019-01-01'),
            ('2020-02-28', '2019-02-28'),
            ('2020-02-29', '2019-02-28'),
            ('2020-12-31', '2019-12-31'),
        ],
    )
    def test_it_subtracts_a_year(self, input_date, expected_date):
        assert (
            str(
                subtract_year(datetime.strptime(input_date, "%Y-%m-%d").date())
            )
            == expected_date
        )
