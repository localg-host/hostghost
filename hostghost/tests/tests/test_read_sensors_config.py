from hostghost.config import get_devices
from hostghost.devices.sensors.energy.pzem004t import PZEM004TSensor
from hostghost.devices.sensors.temperature_humidity.adafruit import (
    AdafruitSensor,
)
from hostghost.devices.sensors.temperature_humidity.dark_sky import (
    DarkSkySensor,
)
from hostghost.devices.sensors.temperature_humidity.dummy import DummySensor


class TestTemperatureSensorConfig:
    def test_config_adafruit_sensors(self, event_loop, caplog):
        sensors_config = {
            'actuators': {},
            'sensors': {
                'local_sensor_1': {
                    'delay': 60,
                    'gpio': 4,
                    'name': 'Temp sensor 1',
                    'sensor': 'AM2302',
                    'type': 'local_temp',
                },
                'local_sensor_2': {
                    'delay': 60,
                    'gpio': 4,
                    'name': 'Temp sensor 2',
                    'sensor': 'AM2302',
                    'type': 'local_temp',
                },
            },
        }
        devices_list = get_devices(sensors_config)
        assert 'local_sensor_1' in devices_list
        assert isinstance(devices_list['local_sensor_1'], AdafruitSensor)
        assert 'local_sensor_2' in devices_list
        assert isinstance(devices_list['local_sensor_2'], AdafruitSensor)
        assert len(caplog.records) == 0

    def test_config_dummy_sensors(self, event_loop, caplog):
        sensors_config = {
            'actuators': {},
            'sensors': {
                'local_sensor_1': {
                    'delay': 60,
                    'name': 'Temp sensor 1',
                    'sensor': 'DUMMY_SENSOR',
                    'type': 'local_temp',
                }
            },
        }
        devices_list = get_devices(sensors_config)
        assert 'local_sensor_1' in devices_list
        assert isinstance(devices_list['local_sensor_1'], DummySensor)
        assert len(caplog.records) == 0

    def test_config_dark_sky_sensors(self, event_loop, caplog):
        sensors_config = {
            'actuators': {},
            'sensors': {
                'local_sensor_1': {
                    'gpio': 4,
                    'name': 'Temp sensor 1',
                    'sensor': 'DARKSKY',
                    'type': 'ext_temp',
                    'delay': 3600,
                    'latitude': 45.7568,
                    'longitude': 4.8309,
                }
            },
        }
        devices_list = get_devices(sensors_config)
        assert 'local_sensor_1' in devices_list
        assert isinstance(devices_list['local_sensor_1'], DarkSkySensor)
        assert len(caplog.records) == 0


class TestEnergySensorConfig:
    def test_config_pzem004t_sensors(self, event_loop, caplog):
        sensors_config = {
            'actuators': {},
            'sensors': {
                'local_sensor_1': {
                    'gpio': 4,
                    'name': 'Temp sensor 1',
                    'sensor': 'PZEM004T',
                    'type': 'local_energy',
                    'com': '/dev/ttyUSB0',
                    'delay': 10,
                }
            },
        }
        devices_list = get_devices(sensors_config)
        assert 'local_sensor_1' in devices_list
        assert isinstance(devices_list['local_sensor_1'], PZEM004TSensor)
        assert len(caplog.records) == 0
