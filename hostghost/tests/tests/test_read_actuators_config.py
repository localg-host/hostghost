from hostghost.config import get_devices
from hostghost.devices.actuators.relay.dummy import DummyRelay
from hostghost.devices.actuators.relay.one_channel_relay import OneChannelRelay


class TestActuatorConfig:
    def test_config_dummy_relay(self, event_loop, caplog):
        sensors_config = {
            'actuators': {
                'local_actuator_1': {
                    'name': 'Relay 1',
                    'actuator': 'DUMMY_RELAY',
                    'type': 'local_relay_off',
                }
            },
            'sensors': {},
        }
        devices_list = get_devices(sensors_config)
        assert 'local_actuator_1' in devices_list
        assert isinstance(devices_list['local_actuator_1'], DummyRelay)
        assert len(caplog.records) == 0

    def test_config_one_channel_relay(self, event_loop, caplog):
        sensors_config = {
            'actuators': {
                'local_actuator_1': {
                    'gpio': 17,
                    'name': 'Relay 1',
                    'actuator': 'RELAY1C',
                    'type': 'local_relay_off',
                }
            },
            'sensors': {},
        }
        devices_list = get_devices(sensors_config)
        assert 'local_actuator_1' in devices_list
        assert isinstance(devices_list['local_actuator_1'], OneChannelRelay)
        assert len(caplog.records) == 0
