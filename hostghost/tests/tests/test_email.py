import pytest

from hostghost.emails.email import Email, parse_email_url
from hostghost.exceptions import InvalidEmailUrlScheme

from ..expected_emails_values import (
    expected_message_below_threshold,
    expected_message_over_threshold,
    expected_subject_below_threshold,
    expected_subject_over_threshold,
    message_below_threshold,
    message_over_threshold,
    updated_message_data_below_threshold,
    updated_message_data_over_threshold,
)


def get_args(call_args):
    """ allows to handle different version of python """
    if len(call_args) == 2:
        args, _ = call_args
    else:
        _, args, _ = call_args
    return args


class TestEmailUrlParser:
    def test_it_raises_error_if_url_scheme_is_invalid(self):
        url = 'stmp://username:password@localhost:587'
        with pytest.raises(InvalidEmailUrlScheme):
            parse_email_url(url)

    def test_it_parses_email_url(self):
        url = 'smtp://test@example.com:12345678@localhost:25'
        parsed_email = parse_email_url(url)
        assert parsed_email['username'] == 'test@example.com'
        assert parsed_email['password'] == '12345678'
        assert parsed_email['host'] == 'localhost'
        assert parsed_email['port'] == 25
        assert parsed_email['use_tls'] is False
        assert parsed_email['use_ssl'] is False

    def test_it_parses_email_url_with_tls(self):
        url = 'smtp://test@example.com:12345678@localhost:587?tls=True'
        parsed_email = parse_email_url(url)
        assert parsed_email['username'] == 'test@example.com'
        assert parsed_email['password'] == '12345678'
        assert parsed_email['host'] == 'localhost'
        assert parsed_email['port'] == 587
        assert parsed_email['use_tls'] is True
        assert parsed_email['use_ssl'] is False

    def test_it_parses_email_url_with_ssl(self):
        url = 'smtp://test@example.com:12345678@localhost:465?ssl=True'
        parsed_email = parse_email_url(url)
        assert parsed_email['username'] == 'test@example.com'
        assert parsed_email['password'] == '12345678'
        assert parsed_email['host'] == 'localhost'
        assert parsed_email['port'] == 465
        assert parsed_email['use_tls'] is False
        assert parsed_email['use_ssl'] is True


class TestEmail:
    @staticmethod
    def email(url='smtp://none:none@0.0.0.0:1025'):
        return Email(parsed_url=parse_email_url(url))

    @staticmethod
    def assert_message(message, expected_subject, expected_message):
        assert message.get('From') == 'no-reply@example.com'
        assert message.get('To') == 'no-reply@example.com'
        assert message.get('Subject') == expected_subject
        assert expected_message in message.as_string()

    def assert_smtp(self, smtp, expected_subject, expected_message):
        assert smtp.send_message.call_count == 1
        message = get_args(smtp.send_message.call_args)[0]
        self.assert_message(message, expected_subject, expected_message)

    @pytest.mark.parametrize(
        "input_msg,expected_msg",
        [
            (message_below_threshold, updated_message_data_below_threshold),
            (message_over_threshold, updated_message_data_over_threshold),
        ],
    )
    def test_it_updates_message_data(self, input_msg, expected_msg):
        assert self.email().update_message_data(input_msg) == expected_msg

    @pytest.mark.parametrize(
        "input_msg,expected_subject,expected_message",
        [
            (
                updated_message_data_below_threshold,
                expected_subject_below_threshold,
                expected_message_below_threshold,
            ),
            (
                updated_message_data_over_threshold,
                expected_subject_over_threshold,
                expected_message_over_threshold,
            ),
        ],
    )
    def test_it_generates_message(
        self, input_msg, expected_subject, expected_message
    ):
        message = self.email().generate_message(input_msg)

        self.assert_message(message, expected_subject, expected_message)

    async def test_it_sends_message(self, mock_smtp):
        await self.email().send(message_over_threshold)

        smtp = mock_smtp.return_value
        smtp.connect.assert_called_once()
        smtp.starttls.not_called()
        self.assert_smtp(
            smtp,
            expected_subject_over_threshold,
            expected_message_over_threshold,
        )

    async def test_it_sends_message_with_ssl(self, mock_smtp):
        await self.email(url='smtp://none:none@0.0.0.0:1025?ssl=True').send(
            message_over_threshold
        )

        smtp = mock_smtp.return_value
        smtp.connect.assert_called_once()
        smtp.starttls.not_called()
        self.assert_smtp(
            smtp,
            expected_subject_over_threshold,
            expected_message_over_threshold,
        )

    async def test_it_sends_message_with_tls(self, mock_smtp):
        await self.email(url='smtp://none:none@0.0.0.0:1025?tls=True').send(
            message_over_threshold
        )

        smtp = mock_smtp.return_value
        smtp.connect.assert_called_once()
        smtp.starttls.called_once()
        self.assert_smtp(
            smtp,
            expected_subject_over_threshold,
            expected_message_over_threshold,
        )
