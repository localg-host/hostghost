class TestAPI:
    async def test_api_get_servers(self, client):
        resp = await client.get('/api/devices/')
        assert resp.status == 200
        data = await resp.json()
        assert len(data['devices']) == 2

        assert data['devices'][0]['slug'] == 'local-actuator-1'
        assert data['devices'][0]['id'] == 'local_actuator_1'
        assert data['devices'][0]['device_type'] == 'actuator'
        assert data['devices'][0]['icon'] == 'fa-toggle-off'
        assert data['devices'][0]['values'] is None
        assert data['devices'][0]['name'] == 'Relay 1'
        assert data['devices'][0]['actuator'] == 'DUMMY_RELAY'
        assert data['devices'][0]['type'] == 'local_relay_off'
        assert data['devices'][0]['gpio'] == 17

        assert data['devices'][1]['slug'] == 'local-sensor-1'
        assert data['devices'][1]['id'] == 'local_sensor_1'
        assert data['devices'][1]['device_type'] == 'sensor'
        assert data['devices'][1]['icon'] == 'fa-thermometer-full'
        assert data['devices'][1]['values'] is None
        assert data['devices'][1]['name'] == 'Temp sensor 1'
        assert data['devices'][1]['sensor'] == 'DUMMY_SENSOR'
        assert data['devices'][1]['type'] == 'local_temp'
        assert data['devices'][1]['delay'] == 60
        assert data['devices'][1]['thresholds'] == {
            'temperature': {
                'max': {'value': 30, 'actuator': 'local_actuator_1'}
            }
        }

    async def test_api_get_sensor_data_no_device(self, client):
        resp = await client.get('/api/devices/local-sensor-10/data/')
        assert resp.status == 404
        data = await resp.json()
        assert data['message'] == 'Error: device not found'
