from collections import OrderedDict
from unittest.mock import patch

import pytest

from hostghost.config import get_devices, get_devices_config
from hostghost.exceptions import InvalidDeviceId


class TestDeviceConfig:
    def test_it_gets_default_config_if_no_pre_existing_config(self, tmpdir):
        default_config = {
            'actuators': {
                'local_actuator_1': {
                    'actuator': 'DUMMY_RELAY',
                    'gpio': 17,
                    'name': 'Relay 1',
                    'type': 'local_relay_off',
                }
            },
            'sensors': {
                'local_sensor_1': {
                    'delay': 60,
                    'name': 'Temp sensor 1',
                    'notifications': False,
                    'sensor': 'DUMMY_SENSOR',
                    'type': 'local_temp',
                    'thresholds': {
                        'temperature': {
                            'max': {
                                'actuator': 'local_actuator_1',
                                'value': 30,
                            }
                        }
                    },
                }
            },
        }
        config = get_devices_config(tmpdir / 'hostghost')
        assert config == default_config

    @patch(
        'hostghost.config.available_devices_modules',
        {
            'AM2302': {
                'module': 'Adafruit_DHT',
                'class': 'AdafruitSensor',
                'class_object': None,
            }
        },
    )
    def test_it_logs_errors_if_device_is_not_supported(self, caplog):
        import sys

        adafruit = sys.modules["Adafruit_DHT"]
        del sys.modules["Adafruit_DHT"]

        devices_config = {
            'actuators': {},
            'sensors': {
                'local_sensor_1': {
                    'delay': 60,
                    'gpio': 9,
                    'name': 'Temp sensor 1',
                    'sensor': 'AM2302',
                    'type': 'local_temp',
                },
            },
        }
        get_devices(devices_config)
        assert len(caplog.records) == 1
        assert caplog.records[0].levelname == 'ERROR'
        assert (
            caplog.records[0].message == 'No module named \'Adafruit_DHT\'. '
            'This module is mandatory for \'AM2302\' sensors.'
        )

        sys.modules["Adafruit_DHT"] = adafruit

    def test_it_logs_errors_for_not_working_devices(self, caplog):
        devices_config = {
            'actuators': {},
            'sensors': {
                'local_sensor_1': {
                    'delay': 60,
                    'gpio': 4,
                    'name': 'Temp sensor 1',
                },
                'local_sensor_2': {
                    'delay': 60,
                    'gpio': 4,
                    'name': 'Temp sensor 2',
                    'sensor': 'NOT_WORKING',
                    'type': 'local_temp',
                },
            },
        }
        devices_list = get_devices(devices_config)
        assert devices_list == OrderedDict()
        assert len(caplog.records) == 2

        assert caplog.records[0].levelname == 'ERROR'
        assert (
            caplog.records[0].message
            == "No type defined for device 'local_sensor_1'"
        )

        assert caplog.records[1].levelname == 'ERROR'
        assert (
            caplog.records[1].message
            == "Device type 'NOT_WORKING' is not supported."
        )

    def test_it_raises_error_if_two_devices_have_the_same_id(self, event_loop):
        sensors_config = {
            'actuators': {
                'local_1': {
                    'name': 'Relay 1',
                    'actuator': 'DUMMY_RELAY',
                    'type': 'local_relay_off',
                }
            },
            'sensors': {
                'local_1': {
                    'delay': 60,
                    'gpio': 4,
                    'name': 'Temp sensor 1',
                    'sensor': 'AM2302',
                    'type': 'local_temp',
                },
            },
        }
        error_message = (
            'a device already exists with id \'local_1\'. '
            'Check config files.'
        )
        with pytest.raises(InvalidDeviceId, match=error_message):
            get_devices(sensors_config)
