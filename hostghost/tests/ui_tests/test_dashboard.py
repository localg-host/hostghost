import os

HOST = os.getenv('HOSTGHOST_HOST', '0.0.0.0')


def test_dashboard(selenium):
    selenium.get(f"http://{HOST}:8889")

    header = selenium.find_element_by_tag_name('header').text
    assert "HostGhost" in header

    h2s = selenium.find_elements_by_tag_name('H2')
    assert "Sensors" in h2s[0].text
    assert "Actuators" in h2s[1].text

    cards = selenium.find_elements_by_class_name('card')

    # dummy temp sensor card
    assert "Temp sensor 1" in cards[0].text
    assert "temperature" in cards[0].text
    assert "time" in cards[0].text

    # dummy temp sensor config details
    device_config = cards[0].find_element_by_class_name('fa-cogs')
    device_config.click()
    assert "Temp sensor 1" in cards[1].text
    assert "local_sensor_1" in cards[1].text
    assert "sensor" in cards[1].text
    assert "DUMMY_SENSOR" in cards[1].text
    assert "thresholds" in cards[1].text
    assert "max: 30°C (actuator: Relay 1)" in cards[1].text

    modal_close = cards[0].find_element_by_class_name('modal-close')
    modal_close.click()

    # dummy relay card
    assert "Relay 1" in cards[2].text
    assert "state" in cards[2].text
    assert "time" in cards[2].text

    # dummy relay config details
    device_config = cards[2].find_element_by_class_name('fa-cogs')
    device_config.click()
    assert "Relay 1" in cards[3].text
    assert "local_actuator_1" in cards[3].text
    assert "actuator" in cards[3].text
    assert "DUMMY_RELAY" in cards[3].text
    assert "thresholds" not in cards[3].text
