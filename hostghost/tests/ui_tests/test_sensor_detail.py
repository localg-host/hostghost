import os

HOST = os.getenv('HOSTGHOST_HOST', '0.0.0.0')


def test_temp_sensor_detail(selenium):
    selenium.get(f"http://{HOST}:8889")

    header = selenium.find_element_by_tag_name('header').text
    assert "HostGhost" in header

    cards = selenium.find_elements_by_class_name('card')
    detail_link = cards[0].find_element_by_tag_name('a')
    detail_link.click()

    title = selenium.find_element_by_tag_name('h1')
    assert 'Temp sensor 1' in title.text

    # dummy temp sensor config details
    device_config = title.find_element_by_class_name('fa-cogs')
    device_config.click()
    assert "Temp sensor 1" in title.text
    assert "local_sensor_1" in title.text
    assert "sensor" in title.text
    assert "DUMMY_SENSOR" in title.text
    assert "thresholds" in title.text
    assert "max: 30°C (actuator: Relay 1)" in title.text

    modal_close = title.find_element_by_class_name('modal-close')
    modal_close.click()
