class AdafruitDHT:
    """
    Note: Adafruit_DHT is only available for Raspberry Pi and BeagleBone.
    It can't be installed on a regular PC.
    """

    def __init__(self):
        self.DHT11 = ('DHT11',)
        self.DHT22 = ('DHT22',)
        self.AM2302 = ('AM2302',)
        self.temperature = 30
        self.humidity = 40

    def read_retry(self, sensor, pin):
        if pin == 12:
            self.temperature = 10
            self.humidity = 120
        return self.humidity, self.temperature


class Weather:
    humidity = 0.35
    temperature = 25


class Forecast:
    @staticmethod
    def currently():
        return Weather()


class ForecastIO:
    @staticmethod
    def load_forecast(*args, **kwargs):
        return Forecast()


class OutputDevice:
    def __init__(self, channel, active_high, initial_value):
        self.channel = channel
        self.active_high = active_high
        self.initial_value = initial_value

    def off(self):
        pass

    def toggle(self):
        ...


class GpioZero:
    """
    Note: gpiozero is working on Raspberry Pi.
    """

    def OutputDevice(self, channel, active_high, initial_value):
        return OutputDevice(channel, active_high, initial_value)
