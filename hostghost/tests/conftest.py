import asyncio
import sys
from unittest.mock import MagicMock, patch
from uuid import uuid4

import pytest
from aiohttp import web

from hostghost import config, routes
from hostghost.devices.actuators.relay.dummy import DummyRelay
from hostghost.emails.email import Email, parse_email_url

from .utils import AdafruitDHT, ForecastIO, GpioZero

sys.modules['Adafruit_DHT'] = AdafruitDHT()
sys.modules['forecastio'] = ForecastIO()
sys.modules['gpiozero'] = GpioZero()


async def async_magic_mock():
    ...


MagicMock.__await__ = lambda x: async_magic_mock().__await__()


@pytest.yield_fixture
def event_loop(loop):
    """
    create an event loop instance for each test case and cancel all
    pending tasks
    """
    loop._close = loop.close
    loop.close = lambda: None
    yield loop
    tasks = (
        asyncio.Task.all_tasks(loop)
        if sys.version.startswith('3.6')
        else asyncio.all_tasks(loop)
    )
    for task in tasks:
        if not (task.done() or task.cancelled()):
            task.cancel()
    loop.close = loop._close


@pytest.fixture
def mock_smtp():
    with patch('hostghost.emails.email.SMTP') as smtp:
        yield smtp


@pytest.fixture
def client(loop, aiohttp_client, mock_smtp):
    app = web.Application()
    config.read('/tmp/pytest/{}'.format(uuid4()))
    app.add_routes(routes.routes)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture
def app_mock_smtp(mock_smtp):
    with patch(
        'hostghost.devices.sensors.sensor.app',
        {
            'email': Email(parse_email_url('smtp://none:none@0.0.0.0:1025')),
            'UI_URL': 'http://localhost:8889',
        },
    ):
        yield mock_smtp


@pytest.fixture
def app_with_actuator():
    class App:
        devices = {}

    actuator = DummyRelay(
        name='local_actuator_1',
        config={
            'gpio': 4,
            'name': 'Relay 1',
            'actuator': 'RELAY_1C',
            'type': 'local_relay_off',
        },
    )
    app = App()
    app.devices['local_actuator_1'] = actuator
    with patch(
        'hostghost.devices.sensors.sensor.app', app,
    ):
        yield actuator
