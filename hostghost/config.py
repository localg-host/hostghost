import importlib
import os
import shutil
from collections import OrderedDict
from os.path import abspath, dirname, isdir

import toml

from hostghost import app, app_log
from hostghost.devices.available_devices import available_devices_modules
from hostghost.exceptions import InvalidDeviceId

app.devices = OrderedDict()
devices_types = []


def get_devices_config(config_dir):
    devices_config = {'actuators': {}, 'sensors': {}}

    if not isdir(config_dir):
        conf_src = os.path.join(dirname(abspath(__file__)), 'etc')
        shutil.copytree(conf_src, config_dir)

    for key in devices_config:
        filepath = os.path.join(config_dir, '{}.toml'.format(key))
        if os.path.exists(filepath):
            devices_config[key] = toml.load(filepath)

    return devices_config


def get_devices(all_devices):
    devices_list = OrderedDict()
    devices_types_list = []
    device_ids_list = []

    for key in all_devices.keys():
        device_type = key[:-1]
        devices = all_devices.get(key)
        for device_id, config in devices.items():

            # check uniqueness of devices id amongst toml files
            if device_id in device_ids_list:
                raise InvalidDeviceId(
                    'a device already exists with id'
                    f' \'{device_id}\'. Check config files.'
                )
            else:
                device_ids_list.append(device_id)

            current_device_type = config.get(device_type)
            app_log.debug(
                f"Loading device '{device_id}' ({device_type} "
                f"type: {current_device_type})"
            )

            if not current_device_type:
                app_log.error(f"No type defined for device '{device_id}'")
                continue

            if current_device_type not in available_devices_modules:
                app_log.error(
                    f"Device type '{current_device_type}' is not supported."
                )
                continue

            device_module = available_devices_modules.get(current_device_type)

            if current_device_type not in devices_types_list:
                try:
                    module = importlib.import_module(
                        device_module.get('module')
                    )
                    available_devices_modules[current_device_type][
                        'class_object'
                    ] = module
                    devices_types_list.append(current_device_type)
                except ImportError as e:
                    app_log.error(
                        f"{e}. This module is mandatory for "
                        f"'{current_device_type}' sensors."
                    )
                    continue
            else:
                module = device_module.get('class_object')

            devices_list[device_id] = getattr(
                module, device_module.get('class')
            )(device_id, config)
    return devices_list


def read(config_dir):
    devices = get_devices_config(config_dir)
    app.devices = get_devices(devices)
