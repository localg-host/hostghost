class InvalidApiKey(Exception):
    ...


class InvalidDeviceId(Exception):
    ...


class InvalidEmailUrlScheme(Exception):
    ...
