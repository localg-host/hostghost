from datetime import date


def subtract_year(end):
    try:
        return end.replace(year=(end.year - 1))
    except ValueError:
        return end + (date(end.year - 1, 3, 1) - date(end.year, 3, 1))
