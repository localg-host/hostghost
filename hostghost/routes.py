import os
from datetime import datetime, timedelta, timezone

import aiohttp
import aiohttp_jinja2
from aiohttp import web

from hostghost import app
from hostghost.utils import subtract_year

routes = web.RouteTableDef()
routes.static('/static/', os.path.join(os.path.dirname(__file__), 'static'))


@routes.view('/{path:(?!api)(?!websocket)(.*)}')
class Index(web.View):
    @aiohttp_jinja2.template('index.html')
    async def get(self):
        return {}


@routes.view('/api/devices/{device_slug}/data/')
class SensorsData(web.View):
    async def get(self):
        device_slug = self.request.match_info['device_slug']
        if device_slug.replace("-", "_") not in app.devices.keys():
            return web.json_response(
                {'message': 'Error: device not found'}, status=404
            )

        query = self.request.query
        data = None
        time_frame = query['time'] if 'time' in query else '24h'

        try:
            end = (
                datetime.fromtimestamp(int(query['end']), timezone.utc)
                if 'end' in query
                else datetime.now(timezone.utc)
            )
            start = (
                datetime.fromtimestamp(int(query['start']), timezone.utc)
                if 'end' in query
                else None
            )
        except ValueError:
            return web.json_response(
                {'message': 'Error: invalid payload on data fetching'},
                status=400,
            )

        if time_frame == '24h':
            start = start if start else end - timedelta(days=1)
            group_by = '1h'
        elif time_frame == '7d':
            start = start if start else end - timedelta(days=7)
            group_by = '1d'
        elif time_frame == '30d':
            start = start if start else end - timedelta(days=30)
            group_by = '1d'
        elif time_frame == '90d':
            start = start if start else end - timedelta(days=90)
            group_by = '1w'
        elif time_frame == '1y':
            start = start if start else subtract_year(end)
            group_by = '1w'
        else:
            start = start if start else end - timedelta(days=1)
            group_by = '1h'

        start = start.replace(minute=0, second=0, microsecond=0)
        if time_frame != '24h':
            start = start.replace(hour=0)
        for device in app.devices.values():
            if device.slug == device_slug:
                data = await device.get_graph_data(
                    start.isoformat(), end.isoformat(), group_by
                )
        return web.json_response(
            {'data': data, 'start': start.timestamp(), 'end': end.timestamp()}
        )


@routes.view('/api/devices/')
class SensorsListApi(web.View):
    async def get(self):
        return web.json_response(
            {
                'devices': [
                    device.serialize() for device in app.devices.values()
                ]
            }
        )


@routes.view('/websocket')
class WebSocket(web.View):
    async def get(self):
        ws = web.WebSocketResponse()
        await ws.prepare(self.request)
        app.websockets.append(ws)
        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.ERROR:
                break
        app.websockets.remove(ws)
        return ws
