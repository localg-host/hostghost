import asyncio

from hostghost import app


class Device:
    """
    Common config for all devices:

    [device_id]                => unique string, used as id (unique in a given
                                  toml file).
                                  must be unique amongst toml files
    name = "device name"       => name displayed on HostGhost
    type = "xxx"               => used as key to display icon
    """

    def __init__(self, device_id, config):
        self.slug = device_id.replace("_", "-")
        self.id = device_id
        self.device_type = None
        self.config = config
        self.loop = asyncio.get_event_loop()
        self.values = None
        self.icons = {}
        self.run_data(first_call=True)

    async def get_data(self):
        raise NotImplementedError("'get_data' method not defined.")

    def run_data(self, first_call=False):
        # if first call, get data for all devices, even actuators (to display
        # current state for actuators)
        self.loop.call_later(
            60 if first_call else self.config.get('delay', 60),
            lambda: self.loop.create_task(self.get_data()),
        )

    async def write_db(self, values, device_time, db):
        data = [
            {
                'measurement': self.id,
                'time': device_time,
                'tags': {
                    'type': self.config.get('type', 'n/a'),
                    self.device_type: self.config.get(self.device_type, 'n/a'),
                },
                'fields': {**values},
            }
        ]
        return await db.write(data)

    async def send_data(self):
        for websocket in app.websockets:
            await websocket.send_json({self.slug: {**self.values}})

    def serialize(self):
        icon = self.icons.get(self.config.get('type'))
        return {
            'slug': self.slug,
            'id': self.id,
            'device_type': self.device_type,
            'icon': icon,
            'values': self.values,
            **self.config,
        }
