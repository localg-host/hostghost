available_devices_modules = {
    # SENSORS
    'DUMMY_SENSOR': {  # for test
        'module': 'hostghost.devices.sensors.temperature_humidity.dummy',
        'class': 'DummySensor',
        'class_object': None,
    },
    'AM2302': {
        'module': 'hostghost.devices.sensors.temperature_humidity.adafruit',
        'class': 'AdafruitSensor',
        'class_object': None,
    },
    'PZEM004T': {
        'module': 'hostghost.devices.sensors.energy.pzem004t',
        'class': 'PZEM004TSensor',
        'class_object': None,
    },
    'DARKSKY': {
        'module': 'hostghost.devices.sensors.temperature_humidity.dark_sky',
        'class': 'DarkSkySensor',
        'class_object': None,
    },
    # ACTUATORS
    'DUMMY_RELAY': {  # for test
        'module': 'hostghost.devices.actuators.relay.dummy',
        'class': 'DummyRelay',
        'class_object': None,
    },
    'RELAY1C': {
        'module': 'hostghost.devices.actuators.relay.one_channel_relay',
        'class': 'OneChannelRelay',
        'class_object': None,
    },
}
