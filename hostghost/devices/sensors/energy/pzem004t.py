import struct
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone

import serial

from hostghost import app, app_log
from hostghost.devices.sensors.sensor import Sensor


def check_checksum(_tuple):
    _list = list(_tuple)
    _checksum = _list[-1]
    _list.pop()
    _sum = sum(_list)
    if _checksum == _sum % 256:
        return True
    else:
        raise Exception("Wrong checksum")


class PZEM004T:
    """
    # source: http://pdacontrolen.com/meter-pzem-004t-with-arduino-esp32-esp8266-python-raspberry-pi/  # noqa

    """

    setAddrBytes = [0xB4, 0xC0, 0xA8, 0x01, 0x01, 0x00, 0x1E]
    readVoltageBytes = [0xB0, 0xC0, 0xA8, 0x01, 0x01, 0x00, 0x1A]
    readCurrentBytes = [0xB1, 0xC0, 0xA8, 0x01, 0x01, 0x00, 0x1B]
    readPowerBytes = [0xB2, 0xC0, 0xA8, 0x01, 0x01, 0x00, 0x1C]

    def __init__(self, com="/dev/ttyUSB0", timeout=10.0):
        self.ser = serial.Serial(
            port=com,
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=timeout,
        )
        if self.ser.isOpen():
            self.ser.close()
        self.ser.open()

    def is_ready(self):
        self.ser.write(serial.to_bytes(self.setAddrBytes))
        rcv = self.ser.read(7)
        if len(rcv) == 7:
            unpacked = struct.unpack("!7B", rcv)
            if check_checksum(unpacked):
                return True
        else:
            raise serial.SerialTimeoutException("Timeout setting address")

    def read_voltage(self):
        self.ser.write(serial.to_bytes(self.readVoltageBytes))
        rcv = self.ser.read(7)
        if len(rcv) == 7:
            unpacked = struct.unpack("!7B", rcv)
            if check_checksum(unpacked):
                tension = unpacked[2] + unpacked[3] / 10.0
                return tension
        else:
            raise serial.SerialTimeoutException("Timeout reading tension")

    def read_current(self):
        self.ser.write(serial.to_bytes(self.readCurrentBytes))
        rcv = self.ser.read(7)
        if len(rcv) == 7:
            unpacked = struct.unpack("!7B", rcv)
            if check_checksum(unpacked):
                current = unpacked[2] + unpacked[3] / 100.0
                return current
        else:
            raise serial.SerialTimeoutException("Timeout reading current")

    def read_power(self):
        self.ser.write(serial.to_bytes(self.readPowerBytes))
        rcv = self.ser.read(7)
        if len(rcv) == 7:
            unpacked = struct.unpack("!7B", rcv)
            if check_checksum(unpacked):
                power = unpacked[1] * 256 + unpacked[2]
                return power
        else:
            raise serial.SerialTimeoutException("Timeout reading power")

    def read_all(self):
        if self.is_ready():
            return (
                self.read_voltage(),
                self.read_current(),
                self.read_power(),
            )

    def close(self):
        self.ser.close()

    def get_data(self):
        try:
            self.read_all()
        finally:
            self.close()


class PZEM004TSensor(Sensor):
    """
    energy sensor, based on PZEM-004T, returning:
    - voltage in Volt
    - current in Ampere
    - power in Watt


    Config example:
    ---------------
    [local_sensor_3]
    name = "Energy sensor 1"
    sensor = "PZEM004T"
    type = "local_energy"
    com = "/dev/ttyUSB0"
    delay = 10
    """

    def __init__(self, name, config):
        super().__init__(name, config)

    async def get_data(self):
        sensor = PZEM004T(com=self.config.get('com', '/dev/ttyUSB0'))
        try:
            with ThreadPoolExecutor(max_workers=2) as executor:
                voltage, current, power = await self.loop.run_in_executor(
                    executor, sensor.read_all
                )

                # energy calculation in kWh:
                # power is measured for 1 second (TODO; to validate)
                # => energy = power / 1000 * time delta in second / 3600
                energy = float((power / 1000) * (1 / 3600))

                app_log.debug(
                    f"Sensor '{self.id}': "
                    f'Voltage={voltage:0.2f}V, Current={current:0.2f}A, '
                    f'Power={power:0.2f}W, Energy={energy:0.6f}kWh'
                )
                sensor_time = datetime.now(timezone.utc)

                voltage_check_thresholds = await self.check_thresholds(
                    voltage, 'voltage'
                )
                current_check_thresholds = await self.check_thresholds(
                    current, 'current'
                )
                power_check_thresholds = await self.check_thresholds(
                    power, 'power'
                )

                self.values = {
                    'voltage': {
                        'text': f'{voltage:0.2f}V',
                        'value': voltage,
                        'exceeds_thresholds': voltage_check_thresholds[
                            'exceeds_thresholds'
                        ],
                    },
                    'current': {
                        'text': f'{current:0.2f}A',
                        'value': current,
                        'exceeds_thresholds': current_check_thresholds[
                            'exceeds_thresholds'
                        ],
                    },
                    'power': {
                        'text': f'{power:0.2f}W',
                        'value': power,
                        'exceeds_thresholds': power_check_thresholds[
                            'exceeds_thresholds'
                        ],
                    },
                    'time': datetime.strftime(
                        sensor_time.replace(tzinfo=timezone.utc).astimezone(
                            tz=None
                        ),
                        '%d/%m/%Y %H:%M:%S',
                    ),
                }
                if voltage_check_thresholds.get('action'):
                    await self.trigger_actuator(
                        voltage_check_thresholds['action']
                    )
                if current_check_thresholds.get('action'):
                    await self.trigger_actuator(
                        current_check_thresholds['action']
                    )
                if power_check_thresholds.get('action'):
                    await self.trigger_actuator(
                        power_check_thresholds['action']
                    )
                await self.write_db(
                    {
                        'voltage': voltage,
                        'current': current,
                        'power': power,
                        'energy': energy,
                    },
                    sensor_time,
                    app['db'],
                )
                await self.send_data()
        except Exception as e:
            app_log.error(f'[get_data] - Sensor \'{self.id}\': {e}')
        finally:
            self.run_data()

    async def get_graph_data(self, start, end, group):
        return await app['db'].query(
            f'SELECT '
            f'INTEGRAL(energy) as energy, '
            f'MIN(power) as power_min, '
            f'MEAN(power) as power_mean, '
            f'MAX(power) as power_max '
            f'FROM {self.id} '
            f'WHERE time >= \'{start}\' and time <= \'{end}\' '
            f'GROUP BY time({group})',
            epoch="s",
        )
