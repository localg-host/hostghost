import Adafruit_DHT

from hostghost.devices.sensors.temperature_humidity.temp_sensor import (
    TempHumiditySensor,
)

ADAFRUIT_SENSORS = {
    'DHT11': Adafruit_DHT.DHT11,
    'DHT22': Adafruit_DHT.DHT22,
    'AM2302': Adafruit_DHT.AM2302,
}


class AdafruitSensor(TempHumiditySensor):
    """
    Adafruit sensor, returning temperature and humidity.
    Works only on Raspberry Pi (and BeagleBone).

    Mandatory configuration parameters:
    - gpio: GPIO pin used to get data from sensor


    Config example:
    ---------------
    [local_sensor_1]
    name = "Temp sensor 1"
    sensor = "AM2302"
    type = "local_temp"
    delay = 60
    gpio = 4
      [local_sensor_1.thresholds.temperature.max]
      value = 24
    """

    def __init__(self, name, config):
        super().__init__(name, config)

    async def get_temp_humidity(self, executor, sensor_time):
        return await self.loop.run_in_executor(
            executor,
            Adafruit_DHT.read_retry,
            ADAFRUIT_SENSORS.get(self.config.get('sensor')),
            self.config.get('gpio'),
        )
