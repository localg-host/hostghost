import os

import forecastio

from hostghost.devices.sensors.temperature_humidity.temp_sensor import (
    TempHumiditySensor,
)
from hostghost.exceptions import InvalidApiKey

API_KEY = os.getenv('DARK_SKY_API')


class DarkSkySensor(TempHumiditySensor):
    """
    Weather sensor gathering data from Dark Sky (former forecast.io)

    Mandatory configuration parameters:
    - latitude
    - longitude


    Config example:
    ---------------
    [external_sensor_1]
    name = "Weather"
    sensor = "DARKSKY"
    type = "ext_temp"
    delay = 3600
    latitude = 45.7568
    longitude = 4.8309
    """

    def __init__(self, name, config):
        super().__init__(name, config)

    def forecast_fn(self, sensor_time):
        if not API_KEY or API_KEY == '':
            raise InvalidApiKey('No api Key for weather sensor')
        return forecastio.load_forecast(
            API_KEY,
            self.config.get('latitude'),
            self.config.get('longitude'),
            time=sensor_time,
            units='si',
        )

    async def get_temp_humidity(self, executor, sensor_time):
        forecast = await self.loop.run_in_executor(
            executor, self.forecast_fn, sensor_time
        )
        weather = forecast.currently()
        return weather.humidity * 100, weather.temperature
