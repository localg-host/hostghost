import random
from datetime import datetime, timezone

from hostghost import app, app_log
from hostghost.devices.sensors.sensor import Sensor


class DummySensor(Sensor):
    """
    Dummy sensor for test (returning a random temperature between 20 and 25°C)


    Config example:
    ---------------
    [local_sensor_2]
    name = "Temp sensor 2"
    sensor = "DUMMY"
    type = "local_temp"
    delay = 60
      [local_sensor_1.thresholds.temperature.max]
      value = 23
      [local_sensor_1.thresholds.temperature.min]
      value = 20
    """

    def __init__(self, name, config):
        super().__init__(name, config)

    async def get_data(self):
        try:
            dummy_value = random.randint(20, 25)
            sensor_time = datetime.now(timezone.utc)
            app_log.debug(f'Sensor \'{self.id}\': Temp={dummy_value:0.1f}°C')

            result_check_thresholds = await self.check_thresholds(
                dummy_value, 'temperature'
            )
            self.values = {
                'temperature': {
                    'text': f'{dummy_value:0.1f}°C',
                    'value': dummy_value,
                    'exceeds_thresholds': result_check_thresholds[
                        'exceeds_thresholds'
                    ],
                },
                'time': datetime.strftime(
                    sensor_time.replace(tzinfo=timezone.utc).astimezone(
                        tz=None
                    ),
                    '%d/%m/%Y %H:%M:%S',
                ),
            }

            if result_check_thresholds.get('action'):
                await self.trigger_actuator(result_check_thresholds['action'])
            await self.write_db(
                {'temperature': dummy_value}, sensor_time, app['db']
            )
            await self.send_data()
        except Exception as e:
            app_log.error(f'[get_data] - Sensor \'{self.id}\': {e}')
        finally:
            self.run_data()

    async def get_graph_data(self, start, end, group):
        return await app['db'].query(
            f'SELECT '
            f'MIN(temperature) as temperature_min, '
            f'MEAN(temperature) as temperature_mean, '
            f'MAX(temperature) as temperature_max '
            f'FROM {self.id} '
            f'WHERE time >= \'{start}\' and time <= \'{end}\' '
            f'GROUP BY time({group})',
            epoch="s",
        )
