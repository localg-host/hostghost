from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timezone

from hostghost import app, app_log
from hostghost.devices.sensors.sensor import Sensor


class TempHumiditySensor(Sensor):
    """
    Generic temp-humidity sensor


    Config example:
    ---------------

    [local_sensor_1]
    name = "Test sensor"
    sensor = "DUMMY_SENSOR"
    type = "local_temp"
    delay = 10
      [local_sensor_1.thresholds.temperature.max]
      value = 10
      actuator = "local_actuator_1"
    """

    def __init__(self, name, config):
        super().__init__(name, config)

    async def get_temp_humidity(self, executor, sensor_time):
        """
        :return: humidity, temperature
        """
        raise NotImplementedError("'get_temp_humidity' method not defined.")

    async def get_data(self):
        try:
            with ThreadPoolExecutor(max_workers=2) as executor:
                sensor_time = datetime.now(timezone.utc)
                humidity, temperature = await self.get_temp_humidity(
                    executor, sensor_time
                )

                if humidity > 100:
                    app_log.error(
                        f'[get_data] - Sensor \'{self.id}\': invalid humidity'
                        f' value ({humidity:0.1f}%), data not saved.'
                    )
                    self.run_data()
                    return

                app_log.debug(
                    f'Sensor \'{self.id}\': Temp={temperature:0.1f}°C '
                    f'Humidity={humidity:0.1f}%'
                )

                temp_check_thresholds = await self.check_thresholds(
                    temperature, 'temperature'
                )
                hum_check_thresholds = await self.check_thresholds(
                    humidity, 'humidity'
                )

                self.values = {
                    'temperature': {
                        'text': f'{temperature:0.1f}°C',
                        'value': temperature,
                        'exceeds_thresholds': temp_check_thresholds[
                            'exceeds_thresholds'
                        ],
                    },
                    'humidity': {
                        'text': f'{humidity:0.1f}%',
                        'value': humidity,
                        'exceeds_thresholds': hum_check_thresholds[
                            'exceeds_thresholds'
                        ],
                    },
                    'time': datetime.strftime(
                        sensor_time.replace(tzinfo=timezone.utc).astimezone(
                            tz=None
                        ),
                        '%d/%m/%Y %H:%M:%S',
                    ),
                }

                if temp_check_thresholds.get('action'):
                    await self.trigger_actuator(
                        temp_check_thresholds['action']
                    )
                if hum_check_thresholds.get('action'):
                    await self.trigger_actuator(hum_check_thresholds['action'])

                await self.write_db(
                    {'temperature': temperature, 'humidity': humidity},
                    sensor_time,
                    app['db'],
                )
                await self.send_data()
        except Exception as e:
            app_log.error(f'[get_data] - Sensor \'{self.id}\': {e}')
        finally:
            self.run_data()

    async def get_graph_data(self, start, end, group):
        return await app['db'].query(
            f'SELECT '
            f'MIN(temperature) as temperature_min, '
            f'MEAN(temperature) as temperature_mean, '
            f'MAX(temperature) as temperature_max, '
            f'MIN(humidity) as humidity_min, '
            f'MEAN(humidity) as humidity_mean, '
            f'MAX(humidity) as humidity_max '
            f'FROM {self.id} '
            f'WHERE time >= \'{start}\' and time <= \'{end}\' '
            f'GROUP BY time({group})',
            epoch="s",
        )
