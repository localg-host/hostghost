from hostghost import app, app_log
from hostghost.devices.device import Device


class Sensor(Device):
    """
    A sensor must define a delay (unlike an actuator).
    It can have thresholds to trigger specific actions (see actuators)


    Common config for all sensors:
    ------------------------------

    - minimal config
    [sensor_id]
    name = "sensor_name"            => name displayed on hostghost
    sensor = "SENSOR_TYPE"          => key in 'available_sensors_modules'
    type = "xxx"                    => key in 'sensors_type_icon'
    delay = 60                      => delay in seconds
    notifications = true            => send a email notification, if a
                                       threshold is exceeded (default: true)

    - with thresholds (=> display a marker on chart)
    [sensor_id]
    name = "sensor_name"
    sensor = "SENSOR_TYPE"
    type = "xxx"
    delay = 60
    notifications = true
      [local_sensor_1.thresholds.temperature.max]
      value = 20

    - with thresholds (=> display a region on chart)
    [sensor_id]
    name = "sensor_name"
    sensor = "SENSOR_TYPE"
    type = "xxx"
    delay = 60
    notifications = true
      [local_sensor_1.thresholds.temperature.max]
      value = 30
      [local_sensor_1.thresholds.temperature.min]
      value = 10

    - with thresholds and actuator
    [sensor_id]
    name = "sensor_name"
    sensor = "SENSOR_TYPE"          => key in 'available_sensors_modules'
    type = "xxx"                    => key in 'sensors_type_icon'
    delay = 60                      => delay in seconds
    notifications = true
      [local_sensor_1.thresholds.temperature.max]
      value = 20                    => value to trigger an actuator
      actuator = "local_actuator_1" => device_id of actuator to trigger
                                       (OFF (state = 0) => ON (state = 1))
    """

    def __init__(self, name, config):
        super().__init__(name, config)
        self.device_type = 'sensor'
        self.threshold_exceeded = {}
        self.icons = {
            "local_temp": "fa-thermometer-full",
            "ext_temp": "fa-thermometer-half",
            "local_energy": "fa-bolt",
        }

    @staticmethod
    def threshold_not_yet_exceeded(previous_threshold_exceeded, threshold):
        return (
            not previous_threshold_exceeded
            or previous_threshold_exceeded != threshold
        )

    async def check_thresholds(self, value, value_type):
        # TODO: refactor
        previous_threshold_exceeded = self.threshold_exceeded.get(value_type)
        result = {'exceeds_thresholds': None}
        thresholds = self.config.get('thresholds')

        if thresholds and thresholds.get(value_type):
            value_thresholds = thresholds.get(value_type)

            if 'min' in value_thresholds:

                if value <= value_thresholds['min']['value']:
                    result['exceeds_thresholds'] = 'min'
                    self.threshold_exceeded[value_type] = 'min'
                    if self.threshold_not_yet_exceeded(
                        previous_threshold_exceeded, 'min'
                    ) and value_thresholds['min'].get('actuator'):
                        result['action'] = value_thresholds['min']['actuator']

                if (
                    previous_threshold_exceeded == 'min'
                    and value > value_thresholds['min']['value']
                ):
                    del self.threshold_exceeded[value_type]
                    if 'actuator' in value_thresholds['min']:
                        result['action'] = value_thresholds['min']['actuator']

            if 'max' in value_thresholds:

                if value >= value_thresholds['max']['value']:
                    result['exceeds_thresholds'] = 'max'
                    self.threshold_exceeded[value_type] = 'max'
                    if self.threshold_not_yet_exceeded(
                        previous_threshold_exceeded, 'max'
                    ) and value_thresholds['max'].get('actuator'):
                        result['action'] = value_thresholds['max']['actuator']

                if (
                    previous_threshold_exceeded == 'max'
                    and value < value_thresholds['max']['value']
                ):
                    del self.threshold_exceeded[value_type]
                    if 'actuator' in value_thresholds['max']:
                        result['action'] = value_thresholds['max']['actuator']

        if self.config.get('notifications', True) and (
            self.threshold_exceeded.get(value_type)
            != previous_threshold_exceeded
        ):
            await self.send_email_notification(
                value,
                value_type,
                self.threshold_exceeded.get(value_type),
                previous_threshold_exceeded,
            )

        return result

    async def get_graph_data(self, start, end, group):
        raise NotImplementedError("'get_graph_data' method not defined.")

    async def trigger_actuator(self, actuator_id):
        try:
            actuator = app.devices[actuator_id]
            await actuator.action_actuator()
        except Exception as e:
            app_log.error(f'[trigger_actuator] - Sensor \'{self.id}\': {e}')

    async def send_email_notification(
        self, value, value_type, threshold, previous_threshold
    ):
        try:
            if app.get('email'):
                message_data = {
                    'name': self.config['name'],
                    'previous_threshold': previous_threshold,
                    'slug': self.slug,
                    'threshold': threshold
                    if threshold
                    else previous_threshold,
                    'threshold_value': self.config['thresholds'][value_type][
                        threshold if threshold else previous_threshold
                    ]['value'],
                    'ui_url': app['UI_URL'],
                    'value': value,
                    'value_type': value_type,
                }
                await app['email'].send(message_data)
        except Exception as e:
            app_log.error(
                f'[send_email_notification] - Sensor \'{self.id}\': {e}'
            )
