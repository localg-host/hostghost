from datetime import datetime, timezone

from hostghost import app, app_log
from hostghost.devices.device import Device


class Actuator(Device):
    """
    Actuators can only have two states:
    - 'ON' (= True)
    - 'OFF' (= False)
    They don't need a delay unlike sensors


    Common config for all actuators:
    --------------------------------

    [actuator_id]
    name = "sensor_name"
    actuator = "SENSOR_TYPE"        => key in 'available_sensors_modules'
    type = "xxx"                    => key in actuator icons
    """

    def __init__(self, name, config):
        super().__init__(name, config)
        self.device_type = 'actuator'
        self.is_active = False
        self.icons = {
            "local_relay_off": "fa-toggle-off",
            "local_relay_on": "fa-toggle-on",
        }

    async def update_actuator_state(self):
        self.is_active = not self.is_active
        app_log.debug(
            f'Sensor \'{self.id}\' state update to \'{self.is_active}\''
        )
        await self.get_data()

    async def action_actuator(self):
        """ must update actuator state """
        raise NotImplementedError("'action_actuator' method not defined.")

    async def get_data(self):
        try:
            actuator_time = datetime.now(timezone.utc)
            app_log.debug(f'Sensor \'{self.id}\': is_active={self.is_active}')

            self.values = {
                'state': {
                    'text': f'{ "ON" if self.is_active else "OFF"}',
                    'value': self.is_active,
                },
                'time': datetime.strftime(
                    actuator_time.replace(tzinfo=timezone.utc).astimezone(
                        tz=None
                    ),
                    '%d/%m/%Y %H:%M:%S',
                ),
            }
            await self.write_db(
                {'is_active': 1 if self.is_active else 0},
                actuator_time,
                app['db'],
            )
            await self.send_data()
        except Exception as e:
            app_log.error(f'[get_data] - Actuator \'{self.id}\': {e}')

    async def get_graph_data(self, start, end, group):
        # TODO: define the right query for on/off display on charts
        return await app['db'].query(
            f'SELECT MEAN(is_active) as active FROM {self.id} '
            f'WHERE time >= \'{start}\' and time <= \'{end}\' '
            f'GROUP BY time({group})',
            epoch="s",
        )
