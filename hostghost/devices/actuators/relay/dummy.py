from hostghost import app_log
from hostghost.devices.actuators.actuator import Actuator


class DummyRelay(Actuator):
    """
    Dummy relay (w/ one channel) for test


    Config example:
    ---------------
    [local_actuator_1]
    name = "Relay 1"
    actuator = "DUMMY_RELAY"
    type = "local_relay_off"
    """

    def __init__(self, name, config):
        super().__init__(name, config)

    async def action_actuator(self):
        """ for test """
        app_log.debug(f'Sensor \'{self.id}\': action !!')
        await self.update_actuator_state()
