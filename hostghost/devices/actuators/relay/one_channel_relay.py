from concurrent.futures import ThreadPoolExecutor

import gpiozero

from hostghost import app_log
from hostghost.devices.actuators.actuator import Actuator


class OneChannelRelay(Actuator):
    """
    Relay with one channel.
    Works only on Raspberry Pi due to use of gpiozero library

    Mandatory configuration parameters:
    - gpio: GPIO pin used to connect relay (IN)


    Config example:
    ---------------
    [local_actuator_1]
    name = "Relay 1"
    actuator = "RELAY1C"
    type = "local_relay_off"
    gpio = 17
    """

    def __init__(self, name, config):
        super().__init__(name, config)
        self.channel = int(config.get('gpio'))

        self.relay = gpiozero.OutputDevice(
            self.channel, active_high=False, initial_value=False
        )
        self.relay.off()
        self.is_active = 0

    def trigger_relay(self, is_active):
        if self.is_active != is_active:
            app_log.debug(f'Actuator \'{self.id}\' triggered!')
            try:
                self.relay.toggle()
                return True
            except Exception as e:
                app_log.error(f'[trigger_relay] - Actuator \'{self.id}\': {e}')
        return False

    async def action_actuator(self):
        try:
            with ThreadPoolExecutor(max_workers=2) as executor:
                has_switched = await self.loop.run_in_executor(
                    executor, self.trigger_relay, not self.is_active
                )
                if has_switched:
                    await self.update_actuator_state()
        except Exception as e:
            app_log.error(f'[action_actuator] - Actuator \'{self.id}\': {e}')
