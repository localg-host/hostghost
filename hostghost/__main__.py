import argparse
import logging
import os

from aiohttp import web
from aioinflux import InfluxDBClient

from hostghost import app, app_log, config, routes
from hostghost.emails.email import Email, parse_email_url
from hostghost.exceptions import InvalidEmailUrlScheme

HOST = os.getenv('HOSTGHOST_HOST', '0.0.0.0')
PORT = os.getenv('HOSTGHOST_PORT', 8889)
UI_URL = os.getenv('HOSTGHOST_UI_URL', f'http://{HOST}:{PORT}')
DB_HOST = os.getenv('HOSTGHOST_INFLUX_HOST', 'localhost')
DB_PORT = os.getenv('HOSTGHOST_INFLUX_PORT', 8086)
DB_DATABASE = 'hostghost'


async def init_influxdb(app):
    db = InfluxDBClient(host=DB_HOST, port=DB_PORT, db=DB_DATABASE)
    app["db"] = db
    await db.create_database(db=DB_DATABASE)


async def close_influxdb(app):
    await app['db'].close()


async def stop_relays(app):
    for device in app.devices.values():
        # replace use of 'isinstance' by accessing class name to avoid
        # to load not compatible packages (some packages can only be run
        # on a Raspberry Pi)
        if device.__class__.__name__ == 'OneChannelRelay':
            app_log.debug(
                f'Stopping device \'{device.id}\' '
                f'({device.device_type} type: '
                f'{device.config.get(device.device_type)})'
            )
            device.relay.off()


def main():
    parser = argparse.ArgumentParser(description='Hosting monitoring')
    default_config_dir = os.path.expanduser('~/.config/hostghost')
    parser.add_argument(
        '--config',
        dest='config_dir',
        default=default_config_dir,
        help='path to the configuration directory',
    )
    parser.add_argument(
        '--debug', dest='debug', action='store_true', help='display debug log'
    )
    args = parser.parse_args()
    app_log.setLevel(logging.DEBUG if args.debug else logging.INFO)

    config.read(args.config_dir)

    app.on_startup.append(init_influxdb)
    app.on_cleanup.append(close_influxdb)

    app.on_cleanup.append(stop_relays)

    try:
        parsed_email_url = parse_email_url(os.getenv('EMAIL_URL'))
    except InvalidEmailUrlScheme:
        app_log.error(
            'Email url not provided or invalid. No notification will be sent.'
        )
        parsed_email_url = None
    app['email'] = (
        Email(parsed_url=parsed_email_url) if parsed_email_url else None
    )
    app['UI_URL'] = UI_URL

    app.add_routes(routes.routes)
    app_log.debug('Listening to http://%s:%i' % (HOST, PORT))

    web.run_app(app, host=HOST, port=PORT)


if __name__ == '__main__':
    main()
