import logging
import os

import aiohttp_jinja2
import jinja2
from aiohttp import web

log_file = os.getenv('HOSTGHOST_LOG')
logging.basicConfig(
    filename=log_file,
    format='%(asctime)s - %(name)s - %(levelname)s - ' '%(message)s',
    datefmt='%Y/%m/%d %H:%M:%S',
)

app_log = logging.getLogger('hostghost')

jinja2_loader = jinja2.FileSystemLoader(
    os.path.join(os.path.dirname(__file__), 'templates')
)
app = web.Application()
aiohttp_jinja2.setup(app, loader=jinja2_loader)
app.websockets = []
