import os
from email.mime.text import MIMEText

from aiosmtplib import SMTP
from urllib3.util import parse_url

from hostghost.exceptions import InvalidEmailUrlScheme

SUBJECT = (
    "[HostGhost] {value_type} {threshold_action} threshold for sensor '{name}'"
)
MESSAGE = """Hi,

The current {value_type} returned by sensor '{name}' {threshold_action} the defined threshold:
- current value: {value},
- threshold '{threshold}': {threshold_value}.

Sensor details: {ui_url}/devices/{slug}

HostGhost
"""


def parse_email_url(email_url):
    parsed_url = parse_url(email_url)
    if parsed_url.scheme != 'smtp':
        raise InvalidEmailUrlScheme()
    credentials = parsed_url.auth.split(':')
    return {
        'host': parsed_url.host,
        'port': parsed_url.port,
        'use_tls': True if parsed_url.query == 'tls=True' else False,
        'use_ssl': True if parsed_url.query == 'ssl=True' else False,
        'username': credentials[0],
        'password': credentials[1],
    }


class Email:
    def __init__(self, parsed_url):
        self.host = parsed_url['host']
        self.port = parsed_url['port']
        self.use_tls = parsed_url['use_tls']
        self.use_ssl = parsed_url['use_ssl']
        self.username = parsed_url['username']
        self.password = parsed_url['password']
        self.sender_email = os.getenv('SENDER_EMAIL', 'no-reply@example.com')
        self.recipient_email = os.getenv(
            'RECIPIENT_EMAIL', 'no-reply@example.com'
        )

    def generate_message(self, message_data):
        message = MIMEText(MESSAGE.format(**message_data))
        message['From'] = self.sender_email
        message['Subject'] = SUBJECT.format(**message_data)
        message['Content-Type'] = 'text/plain; charset="UTF-8"'
        message['To'] = self.recipient_email
        return message

    @staticmethod
    def update_message_data(message_data):
        message_data['threshold_action'] = (
            'drops below' if message_data['previous_threshold'] else 'reaches'
        )
        return message_data

    async def send(self, message_data):
        message = self.generate_message(
            message_data=self.update_message_data(message_data)
        )
        smtp_client = SMTP(
            hostname=self.host,
            port=self.port,
            use_tls=self.use_ssl,
            start_tls=self.use_tls,
            username=self.username,
            password=self.password,
        )
        await smtp_client.connect()
        await smtp_client.send_message(message)
        await smtp_client.quit()
