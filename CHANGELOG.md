# Change log

## Version 0.1.0a7 (2020/07/12)

#### New Features

* [#13](https://gitlab.com/localg-host/hostghost/issues/13) - display data over a year on device detail
* [#12](https://gitlab.com/localg-host/hostghost/issues/12) - indicate min and max values over period displayed on chart


## Version 0.1.0a6 (2020/07/11)

#### New Features

* [#11](https://gitlab.com/localg-host/hostghost/issues/11) - exclude invalid data from Adafruit sensor


## Version 0.1.0a5 (2020/06/01)

#### New Features

* [#9](https://gitlab.com/localg-host/hostghost/issues/9) - display area for min and max values on charts 
* [#7](https://gitlab.com/localg-host/hostghost/issues/7) - add notification system 

### Misc

- Environment variable `HOSTGHOST_INFLUX_DB` has been renamed `HOSTGHOST_INFLUX_HOST`
- Frappe Chart has been replaced by Chart.js

## Version 0.1.0a4 (2020/01/29)

#### New Features

* [#6](https://gitlab.com/localg-host/hostghost/issues/6) - parametrize hostghost configuration 
* [#4](https://gitlab.com/localg-host/hostghost/issues/4) - add documentation 

#### Bugs Fixed

* [#8](https://gitlab.com/localg-host/hostghost/issues/8) - the first data displayed is incomplete
* [#5](https://gitlab.com/localg-host/hostghost/issues/5) - interface for pzem004T should be in sensor config

#### Misc

* disable charts for actuators for now
* update service file

## Version 0.1.0a3 (2019/07/28)

#### New Features

* Add actuators:
   * 1 channel relay
   * dummy relay for tests
* Display device config in a modal
* Add thresholds on sensor config and displayed them on charts (thresholds can be hidden)
* Exceeding threshold can trigger an actuator, if defined in sensor config
* Device uuid removed and replaced by device slug (since toml file implies unique identifiers) 

#### Bugs Fixed

* Remove unwanted 'static' in app path

#### Misc

* uses a [forked version](https://github.com/SamR1/vue2-frappe) of Frappe Chart wrapper for Vue (until new release):
    * markers and regions display fix (see [PR](https://github.com/JustSteveKing/vue2-frappe/pull/24))


## Version 0.1.0a2 (2019/06/23)

#### New Features

* New sensors:
    * Energy sensor (PZEM004T)
    * Weather sensor (DarkSky.io)
* Display if HostGhost is disconnected
* Store the last sensor values

#### Bugs Fixed

* Chart fixes (see [Frappe Chart fork](https://github.com/SamR1/charts)):
    * fix zero values display
    * hide bar when value is null
    * fix x axis to better display minor data variations


## Version 0.1.0a1 (2019/06/02)

First release

#### Features
* Dashboard displaying current temperature and humidity from Adafruit sensor (AM2302)
* Sensor detail with a chart displaying data according to following time frames: 24h, 7d, 30d and 90d
* Dummy temperature sensor for tests

#### Misc
* WatchGhost-inspired application 
* HostGhost uses a [forked version](https://github.com/SamR1/charts) of [Frappe Chart](https://frappe.io/charts):
    * add class name to axis labels
    * enable to define color for each dataset
    * adjust y-axis when min value is different from zero
    * minor fix on animations
